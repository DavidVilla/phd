#include "PropertyType.ice"

module PropertyService {
  struct Property {
    string propertyName;
    P::T propertyValue;
  };

  enum PropertyModeType {
    NORMAL,
    READONLY,
    FIXEDNORMAL,
    FIXEDREADONLY,
    UNDEFINED
  };

  struct PropertyDef {
    P::T propertyValue;
    PropertyModeType propertyMode;
  };

  dictionary<string,P::T> Properties;
  dictionary<string,PropertyDef> PropertyDefs;
  dictionary<string,PropertyModeType> PropertyModes;

  interface PropertyNamesIterator;
  interface PropertiesIterator;
  interface PropertySetFactory;
  interface PropertySetDef;
  interface PropertySet;

  exception ConstraintNotSupported {};
  exception InvalidPropertyName {};
  exception ConflictingProperty {};
  exception PropertyNotFound {};
  exception UnsupportedTypeCode {};
  exception UnsupportedProperty {};
  exception UnsupportedMode {};
  exception FixedProperty {};
  exception ReadOnlyProperty {};

  enum ExceptionReason {
    rInvalidPropertyName,
    rConflictingProperty,
    rPropertyNotFound,
    rUnsupportedTypeCode,
    rUnsupportedProperty,
    rUnsupportedMode,
    rFixedProperty,
    rReadOnlyProperty
  };

  struct PropertyException {
    ExceptionReason reason;
    string failingPropertyName;
  };

  sequence<PropertyException> PropertyExceptions;

  exception MultipleExceptions {
    PropertyExceptions exceptions;
  };

  interface PropertySetFactory {
    PropertySet* createPropertySet();

    PropertySet* createConstrainedPropertySet(Properties allowedProperties)
      throws ConstraintNotSupported;

    PropertySet* createInitialPropertySet(Properties initialProperties)
      throws MultipleExceptions;
  };

  interface PropertySetDefFactory {
    PropertySetDef* createPropertySetDef();
    PropertySetDef* createConstrainedPropertySetDef(\
      PropertyDefs allowedPropertyDefs)
        throws ConstraintNotSupported;

    PropertySetDef* createInitialPropertySetDef(\
      PropertyDefs initialPropertyDefs)
        throws MultipleExceptions;
  };

  interface PropertySet {
    void defineProperty(string theName, P::T theValue)
      throws InvalidPropertyName, ConflictingProperty, UnsupportedTypeCode,
      UnsupportedProperty, ReadOnlyProperty;

    void defineProperties(Properties nproperties)
      throws MultipleExceptions;

    long getNumberOfProperties();

    void getAllPropertyNames(long howMany, out Ice::StringSeq theNames,
			     out PropertyNamesIterator* rest);

    P::T  getPropertyValue(string theName)
      throws PropertyNotFound, InvalidPropertyName;

    bool getProperties(Ice::StringSeq theNames, out Properties nproperties);

    void getAllProperties(long howMany, out Properties nproperties,
			   out PropertiesIterator* rest);

    void deleteProperty(string theName)
      throws PropertyNotFound, InvalidPropertyName, FixedProperty;

    void deleteProperties(Ice::StringSeq theNames)
      throws MultipleExceptions;

    bool deleteAllProperties();
    bool isPropertyDefined(string theName) throws InvalidPropertyName;
  };

  interface PropertySetDef extends PropertySet {
    void getAllowedProperties(out PropertyDefs allowedDefs);

    void definePropertyWithMode(string theName, P::T theValue,
				PropertyModeType theMode)
      throws InvalidPropertyName, ConflictingProperty, UnsupportedTypeCode,
		    UnsupportedProperty, UnsupportedMode, ReadOnlyProperty;

    void definePropertiesWithModes(PropertyDefs theDefs)
      throws MultipleExceptions;

    PropertyModeType getPropertyMode(string theName)
      throws PropertyNotFound, InvalidPropertyName;

    bool getPropertyModes(Ice::StringSeq theNames, out PropertyModes theModes);

    void setPropertyMode(string theName, PropertyModeType theMode)
      throws InvalidPropertyName, PropertyNotFound, UnsupportedMode;

    void setPropertyModes(PropertyModes theModes)
      throws MultipleExceptions;
  };

  interface PropertyNamesIterator{
    void reset();
    bool nextOne(out string theName);
    bool nextN(long howMany, out Ice::StringSeq theNames);
    void destroy();
  };

  interface PropertiesIterator {
    void reset();
    bool nextOne(out Property aProperty);
    bool nextN(long howMany, out Properties nProperties);
    void destroy();
  };
};
