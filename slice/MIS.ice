#include "PropertyService.ice"

module MIS {
  dictionary<string, string> AttrDict;
  sequence<string> list;

  // for interdomain interoperability
  interface DomainTranslator{
    string translateSv(string serviceID, string orgDomain, string dstDomain);

    // return attribute and type
    AttrDict attributesOf(string serviceID, string domain);
    AttrDict translateAttr(string serviceID, AttrDict orgAttr,
			   string orgDomain, string dstDomain);
  };

  // for MetaModelProperty explorer
  interface Metamodel{
    PropertyService::PropertySetDef* getProperties(string service);
  };
};
