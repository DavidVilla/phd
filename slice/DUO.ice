#include <Ice/BuiltinSequences.ice>
#include <Ice/Identity.ice>
#include <IceStorm/IceStorm.ice>

module DUO {
  dictionary<string, Object*> ObjectPrxDict;

  module Pulse {
    interface W { void set(Ice::Identity oid); };
  };

  module IBool {
    interface R { idempotent bool get(); };
    interface W { set(bool v, Ice::Identity oid); };
  };

  module IByte {
    interface R { idempotent byte get(); };
    interface W { void set(byte v, Ice::Identity oid); };
  };

  module IInt {
    interface R { idempotent int get(); };
    interface W { void set(int v, Ice::Identity oid); };
  };

  module ILong {
    interface R { idempotent long get(); };
    interface W { void set(long v, Ice::Identity oid); };
  };

  module IFloat {
    interface R { idempotent float get(); };
    interface W { void set(float v, Ice::Identity oid); };
  };

  module IString {
    interface R { idempotent string get(); };
    interface W { void set(string v, Ice::Identity oid); };
  };

  module IByteSeq {
    interface R { idempotent Ice::ByteSeq get(); };
    interface W { void set(Ice::ByteSeq v, Ice::Identity oid); };
  };

  module IObject {
    interface R { idempotent Object* get(); };
    interface W { void set(Object* v, Ice::Identity oid); };
  };


  module Func {
    interface Relative {
      void inc(short nsteps);
    };
  };

  module Container {
    exception AlreadyExistsException { string key; };
    exception NoSuchKeyException { string key; };
    exception OperationFailed { string reason; };

    interface RW;
    interface R { ["freeze:read", "ami"] idempotent ObjectPrxDict list(); };
    interface W {
      // Add and remove external items
      ["freeze:write", "ami"]
	void link(string key, Object* value) throws AlreadyExistsException;

      ["freeze:write", "ami"]
	void unlink(string key) throws NoSuchKeyException;

      // Create new Container's in this object
      ["freeze:write", "ami"]
	Container::RW* create(string key) throws AlreadyExistsException;

      // Destroy *this* object
      ["freeze:write", "ami"]
	void destroy();
    };
    interface RW extends R,W {};
  };


  interface Component {
    Ice::StringSeq getAllFacets();
  };

  module Composite {

    // Valid operations for byte, int, float, long
    const string MIN = "minimum";  // lowest value
    const string MAX = "maximum";  // highest value
    const string AVG = "average";  // sum(0..n)/n
    const string MED = "median";   // sort(0..n)[n/2]
    const string HEI = "height";   // max-min

    // Valid operations for bool
    const string ANY = "any";      // true if any true
    const string ALL = "all";      // true if all true

    // Exceptions
    exception InvalidTypeException { string tid; };
    exception NoSuchObjectException { string oid; };

    // Public Factory to create Composites
    interface Factory {
      Object* create(string scalarType) throws InvalidTypeException;
      void destroy(Object* proxy) throws NoSuchObjectException;
      Ice::StringSeq getAllowedTypes();
    };

    interface R extends DUO::Container::R, DUO::Component{};
    interface W extends DUO::Container::RW, DUO::Component{};
  };

  module Active {
    interface R {
      Object* getCb();
      IceStorm::Topic* getTopic();
    };
    interface W {
      void setCbTopic(Object* publisher, IceStorm::Topic* topic);
    };
  };
};
