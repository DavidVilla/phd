#include <Ice/BuiltinSequences.ice>
#include <Ice/Identity.ice>
#include "PropertyService.ice"

module ASD  {
  interface Listener {
    idempotent void adv(Object* prx);
    idempotent void bye(Ice::Identity oid);
  };

  interface Search {
    idempotent void lookup(Listener* cb, string tid,
			   PropertyService::Properties query);
    idempotent void discover(Listener* cb);
  };

  interface PropHldr {
    idempotent PropertyService::PropertySetDef* getp();
  };
};
