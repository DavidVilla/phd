#include <Ice/BuiltinSequences.ice>

module P {
  class T {};
  class BoolT extends T      { bool value; };
  class ByteT extends T      { byte value; };
  class ByteSeqT extends T   { Ice::ByteSeq value; };
  class IntT extends T       { int value; };
  class FloatT extends T     { float value; };
  class StringT extends T    { string value; };
  class StringSeqT extends T { Ice::StringSeq value; };
  class ObjectT extends T    { Object* value; };
};
