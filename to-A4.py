#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, re, os


replaces = [
    ("\documentclass[b5paper,10pt,twoside,openright,pdftex]{book}",
     "\documentclass[a4paper,12pt,twoside,openright,pdftex]{book}"),
    ("\usepackage[body={12.5cm,20cm},left=3cm, top=2.5cm]{geometry} % B5",
     "\usepackage[body={14.5cm,24cm},left=4cm, top=  3cm]{geometry} % A4"),
    ("\singlespacing", "\doublespacing"),
    ]

main = file("main.tex")
A4 = file("main-A4.tex", "w")

content = main.read()

for old,new in replaces:
    content = content.replace(old, new)

A4.write(content)

main.close()
A4.close()


