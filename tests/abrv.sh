# DESC: Abreviaturas {}
# -CMD: egrep -n --color '[[:alpha:]]\{\}[,\.]' $testdir/*.tex # espacios innecesarios
# -CMD: egrep -n --color '\\mws? |\\mws?$$' $testdir/*.tex # mw
# -CMD: egrep -n --color '\\picos? |\\picos?$$' $testdir/*.tex # pico
# -CMD: egrep -n --color '\\bytecodes? |\\bytecodes?$$' $testdir/*.tex # bytecode
# -CMD: egrep -n --color '\\mCORBA |\\mCORBA$$' $testdir/*.tex # mCORBA
# -CMD: egrep -n --color '\\TP |\\TP$$' $testdir/*.tex # TP
# -CMD: egrep -n --color '\\picoGrid |\\picoGrid$$' $testdir/*.tex # picoGrid
