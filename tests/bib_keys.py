# -*- coding: utf-8 -*-
# DESC: Claves de bibliografía repetidas
# +CMD: ls $testdir/main.bib             # pre: main.bib exists
# +CMD: python $file $testdir/main.bib

import sys, re, os

error = 0
keys = []

re_obj = re.compile(r"""\s*key\s*=\s*\{(\w+)\}""")

bib = file(sys.argv[1])
for line in bib:
    match_obj = re_obj.search(line)
    if not match_obj: continue

    key = match_obj.group(1)
    if key in keys:
        error = 1
        print "ERROR: Clave repetida: '%s'" % key
        print os.system("grep %s %s/bib/*" % (key, os.environ['TESTDIR']))

    keys.append(key)

bib.close()
sys.exit(error)
