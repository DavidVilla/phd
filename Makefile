# it requires arco-authors

DPI=300
RUBBER_FLAGS=-m minitoc

all:: main.pdf
	if type notify-send; then \
	    notify-send -h int:transient:1 "Documento compilado";  \
	fi

main.pdf: main.bib thesis.sty main.bib main.gls.aux

main.bib: $(wildcard bib/*.bib)
	cat $^ > $@

main.gls.aux: main.gloss.gen
	bibtex $@

main.gloss.gen: main.tex $(FIGURES) $(TEXSRC)
	pdflatex -draftmode -interaction batchmode $<
	touch $@

vclean::
	$(RM) main.bib main.gls.aux main.mtc*

include arco/latex.mk
