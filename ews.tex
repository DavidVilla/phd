% -*- coding: utf-8 -*-

\chapter{Servicios Web empotrados}
\hyperlabel{chap:ews}
\minitocbox


Los denominados Servicios Web (en adelante \acs{WS}) han
tenido un gran auge en los últimos años gracias en buena parte al
apoyo de la industria. Aunque siguen sufriendo grandes carencias y
desventajas respecto a los \ac{MDOO}
tradicionales~\cite{jong02:_web_servic_soap_corba,
  Gokhale02Reinventing, Henning09ChoosingMiddleware}, su relevancia en
la comunidad investigadora y en el desarrollo profesional de sistemas
distribuidos no puede ser ignorada.

Los protocolos binarios como \ac{GIOP} o \ac{IceP}, habituales en los
\ac{MDOO}, permiten aprovechar al máximo las posibilidades de los
\picos{} puesto que los mensajes requeridos son mucho más pequeños y
regulares que sus equivalentes funcionales en protocolos como
\ac{XML-RPC} o \acx{SOAP}, basados en \ac{XML}.

A pesar de que consideramos que los protocolos basados en \ac{XML} son
mucho más ineficientes que sus equivalentes binarios en términos de
memoria, energía y ancho de banda, en este capítulo se introduce un
mecanismo para conseguir implementaciones de \ac{WS} aptas para
dispositivos empotrados, con niveles de autonomía equiparables a los
que exigimos a los \picos, aunque obviamente con requerimientos de
memoria mayores que aquellos.

Nuestra intención con esta propuesta es proporcionar una alternativa
viable para la construcción de \ac{WS} autónomos empotrados, cuando
las necesidades concretas de la aplicación lo impongan ya sea como
medio para la integración con un sistema existente o como requisito
del cliente. En otro caso, aconsejamos utilizar un \mw{} basado en un
protocolo binario, especialmente en el caso de nodos de una red
\ac{SAN}, ya que ello permite la utilización de \picos, más
eficientes, con menos consumo y adecuados para dispositivos más
sencillos. A los servicios web implementados con las directrices que
se explican a continuación los denominamos \acx{EWS}.

Como en el caso de los \picos, los \ac{EWS} consiguen un alto grado de
interoperabilidad con sistemas basados en \ac{XML-RPC} o \ac{SOAP} con
el único requisito de pasarelas sencillas, sin necesidad de delegados
como entidades software residentes en ellas.


\section{Introducción}

Aunque es posible conseguir interoperabilidad a nivel de red mediante
implementaciones reducidas de \ac{TCP/IP}, en la capa de aplicación
todavía puede ser un grave problema~\cite{Priyantha08tinyweb}. Los
\ac{WS} aparecen como una solución interoperable, independiente del
lenguaje y la plataforma para acceder a los servicios de una red
\ac{SAN} a través de Internet. Ello puede implicar una serie de
interesantes ventajas que veremos a continuación.

El uso de un protocolo como \ac{SOAP}~\cite{SOAP} dentro de una red
\ac{SAN} introduce una sobrecarga importante comparado con los
protocolos binarios. Sin embargo, en algunos escenarios en los que el
consumo de energía o el ancho de banda no son aspectos críticos puede
ser admisible. En este caso, supondremos una instalación de control y
diagnóstico para luces de emergencia en grandes edificios. En este
escenario:

\begin{itemize}
\item Los nodos no tienen problemas de energía porque están conectados
  a la red eléctrica.
\item Los nodos no se mueven.
\item Utilizan comunicaciones inalámbricas para no depender de
  infraestructuras de comunicaciones ajenas al sistema. Esta red tiene
  poco tráfico, se utiliza únicamente para proporcionar acceso al
  diagnóstico de los nodos. Como ésta es una tarea planificada no es
  previsible que se produzca congestión ni grandes requerimientos de
  ancho de banda.
\item El procedimiento de configuración debería ser lo más simple
  posible para reducir el coste de la instalación. En principio será
  posible una configuración remota como un servicio para todo
  el sistema.
\end{itemize}

En este caso, o cualquier otro con características similares, los \ac{WS}
pueden ser una buena alternativa como solución al problema para
proporcionar interoperabilidad en la capa de aplicación con sistemas
externos preexistentes.


\section{Servicios Web en la \acs{SAN}}

La mayoría de las propuestas previas toman datos directamente de la
\ac{WSN} usando protocolos propietarios y los exportan a través de
\ac{WS} residentes en la
pasarela~\cite{Kobialka07OpenSensor,archrock}. El \ac{WS} reside
realmente en la pasarela y no en los nodos sensores. Cada nueva
aplicación suele requerir nuevos desarrollos, y cada \ac{WS} se
convierte en un adaptador o \emph{envoltorio} para un protocolo
binario usado en la \ac{SAN}.

Nuestro enfoque pretende utilizar pasarelas genéricas (independientes
de los dispositivos concretos que forman la red de sensores)
reduciendo todo lo posible los procedimientos de configuración para
conseguir que la información de los nodos esté accesible como
\ac{WS}. La intención (como en ocasiones anteriores) es eliminar la
necesidad de intermediarios a nivel de aplicación tal como el Sensor
Collection Service descrito en~\cite{Kobialka07OpenSensor}.

En~\cite{Al-Yasiri07Data} se presenta un entorno para dar soporte al
uso de \ac{SOAP} en las \ac{WSN} con especial énfasis en la reducción
de la sobrecarga (p.ej. reduciendo el número de mensajes) utilizando
técnicas de agregación de datos. Su implementación se ha probado en el
simulador NS2, pero no se aporta información sobre una implementación
en dispositivos reales.

Otras propuestas~\cite{Helander05SecWeb} intentan empotrar la
arquitectura de \ac{WS} en dispositivos de bajo coste reduciendo el
tamaño de la pila de protocolos (\ac{TCP/IP}, \ac{XML}, \ac{SOAP},
etc.). Esos dispositivos de \emph{bajo coste} exceden, sin embargo, la
capacidad media de la mayoría de las plataformas para redes de
sensores inalámbricas.

\ac{EWS} propone una forma diferente de empotrar servicios web mínimos
en dispositivos \ac{WSN} siguiendo un enfoque muy similar a los
\picos. En lugar de reducir el tamaño de implementaciones existentes
de la pila de protocolos empleada en \ac{WS}, definiremos el conjunto
mínimo de prestaciones que necesita proporcionar un \ac{WS} para  que
pueda seguir considerándose como tal, y partiremos de ese punto para
añadir funcionalidad adicional cuando se requiera y sea posible.


\section{Un enfoque ascendente}
\label{sec:ews:bottomup}

Aunque es importante que cada dispositivo se comporte como un \ac{WS},
no es imprescindible la existencia de una implementación completa de
\ac{TCP/IP}, un servidor web o un reconocedor de \ac{XML}. Si al
recibir peticiones predefinidas, los dispositivos son capaces de
generar respuestas coherentes el sistema puede funcionar tal como se
espera. Para una especificación \ac{WSDL}~\cite{WSDL} dada, los
mensajes de petición y respuesta están perfectamente definidos por el
protocolo de transporte elegido (\ac{SOAP} o \ac{XML-RPC}).

Veamos una petición \ac{SOAP} encapsulada en un mensaje \ac{HTTP}
(listado~\ref{code:SOAP-request}) y su correspondiente respuesta para un
protocolo de interacción muy simple similar a \ac{DUO}: leer el estado
de una luz de emergencia.  Se muestran mensajes \ac{SOAP} completos, a
fin de que el comportamiento descrito en la figura~\ref{fig:EWS-fsm}
resulte más fácil de comprender.

\begin{listing}[
  language = XML,
  caption =  {Ejemplo de petición \acs{SOAP}},
  label =    code:SOAP-request]
POST / HTTP/1.0
Host: localhost:8080
User-agent: SOAPpy 0.12.0
Content-type: text/xml; charset="UTF-8"
Content-length: 336
SOAPAction: "get"

<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope
  SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
  xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/"
  xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
<SOAP-ENV:Body>
<get SOAP-ENC:root="1">
</get>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>
\end{listing}

Es fácil comprobar que \ac{SOAP} resulta un protocolo bastante verboso
en el que la mayoría de la información útil se encuentra en el
elemento \id{Body}. Este mensaje se puede obtener a partir de la
especificación \ac{WSDL} del listado~\ref{code:EWS-WSDL}.

\begin{listing}[
  language = XML,
  caption = {Especificación \acs{WSDL} para el ejemplo descrito},
  label   = code:EWS-WSDL]
[...]
 <message name="getRequest">
  </message>

  <message name="getResponse">
    <part name="retval" type="xs:boolean" />
  </message>
[...]
<operation name="get">
      <input message="getRequest" />
      <output message="getResponse" />
</operation>
[...]
\end{listing}

Podemos implementar un reconocedor ad hoc tomando la especificación
\ac{WSDL} como punto de partida. Este reconocedor puede construir la
respuesta adecuada a partir de plantillas
(listado~\ref{code:SOAP-reply}). Puede verse fácilmente la
equivalencia con las propuestas realizadas para los \picos{} en el
capítulo~\ref{chap:picoObjetos} si bien es necesario puntualizar
algunas diferencias que se verán a continuación.

\begin{listing}[
  language = XML,
  caption = {Respuesta \acs{SOAP} sintetizada},
  label =   code:SOAP-reply]
HTTP/1.0 200 OK
Content-type: text/xml; charset="UTF-8"
Content-length: 501

<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope
  SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
  xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/"
  xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance"
  xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
  xmlns:xsd="http://www.w3.org/1999/XMLSchema">
<SOAP-ENV:Body>
<getResponse SOAP-ENC:root="1">
<Result xsi:type="xsd:boolean">False</Result>
</getResponse>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>
\end{listing}

Como en los capítulos precedentes, nuestro objetivo sigue siendo
cubrir la funcionalidad de los transductores más habituales en las
redes \ac{SAN} y por esa razón planteamos un conjunto de interfaces
\ac{WSDL} similar a \ac{DUO} siguiendo el mismo diseño
\fg{data-centric}. Por supuesto, ésta es una estrategia para maximizar
las posibilidades de implementación en dispositivos con graves
limitaciones de memoria, pero como en aquel caso, no se trata de una
restricción y es posible utilizar interfaces más complejas, asumiendo
el coste correspondiente.

El objetivo es minimizar la cantidad de memoria requerida para
reconocer el conjunto de los posibles mensajes de entrada. Para
conseguirlo, se emplean varias técnicas:

\begin{itemize}
\item Se genera una máquina de estados para reconocer una petición y
  construir una respuesta. El reconocedor ignora varios elementos
  sintácticos irrelevantes, como los espacios en blanco, saltos de
  línea, comentarios \ac{XML}, etc.
\item Nunca se almacenan ni comparan las etiquetas \ac{XML}. En lugar
  de eso, se calculan \fg{checksums} según el algoritmo \ac{CCITT}-\ac{CRC}
  16 bits. El reconocedor únicamente compara resultados del \ac{CRC}
  obtenidos a partir del mensaje con los almacenados en el
  dispositivo. Éstos fueron precalculados en tiempo de generación del
  autómata.
\item Nunca se almacenan mensajes completos. Cada mensaje es analizado
  de forma incremental tal como se recibe, de manera similar a como
  haría un reconocedor \acx{SAX}.
\end{itemize}

Como ejemplo, para el mensaje de petición del
listado~\ref{code:SOAP-request}, se genera una máquina de estados
similar a la de la figura~\ref{fig:EWS-fsm}. Se calcula un \ac{CRC}
para cada etiqueta \ac{XML} (delimitadas por `\code{<>}'). Desde el
estado \id{init} se reconoce la entrada ignorando la cabecera
\ac{HTML}. Esa cabecera no incluye información relevante desde el
punto de vista de los nodos de la \ac{SAN}.

El reconocedor ignora todos los datos de entrada hasta la llegada de
la etiqueta \code{<?xml \... ?>}. Cuando ha llegado completamente se
comprueba el \fg{checksum} almacenado y se produce la transición al estado
\id{Start} que decidirá entre los posibles mensajes de entrada. La
entrada incluye algunas etiquetas de codificación seguidas de la
operación que se desea invocar (\function{get} o \function{set} en el
ejemplo). Dependiendo del \ac{CRC} obtenido en las \emph{posiciones}
esperadas del flujo de entrada la transición lleva al estado en el que
se leen los argumentos de la operación. Esas posiciones quedan
definidas por el punto del flujo en el que aparece alguno de los
caracteres `\code{<>}'. Las operaciones sin argumentos de entrada
implican una transición directa al estado \id{Tail}. En este estado se
valida el resto del mensaje de petición y se invoca el procedimiento
de usuario correspondiente (el sirviente).

\begin{figure}[ht]
\begin{center}
\includegraphics[width=\textwidth]{EWS-fsm.pdf}
\caption{Máquina de estados simplificada para los mensajes \acs{SOAP}
  \function{get} y \function{set}}
\label{fig:EWS-fsm}
\end{center}
\end{figure}

Las operaciones con parámetros de entrada pasan por estados
intermedios. Para el ejemplo, el estado \id{Req Set} reconoce y
almacena temporalmente los parámetros asociados a la operación
\function{set} antes de disparar la transición al estado
\id{Tail}. Por último, después de comprobar la validez del resto del
mensaje, se genera el mensaje de respuesta. Nótese que no es necesario
almacenar nada del método invocado puesto que la cola de cada petición
es diferente y se puede utilizar para discriminar entre los
generadores de respuestas.

En cada estado de generación de respuestas (\id{Resp Set} y
\id{Resp Get} en el ejemplo), el usuario debe proporcionar una rutina
que se ejecuta como consecuencia del mensaje reconocido y genera la
respuesta apropiada.

Aunque por legibilidad no se indica explícitamente en la
figura~\ref{fig:EWS-fsm}, cuando alguna etiqueta de entrada no
corresponde con el \fg{checksum} previsto, la máquina de estados retorna al
estado inicial y cierra la conexión. Eso proporciona una protección
sencilla pero razonable contra clientes defectuosos.

\subsection{Compilación}
\label{sec:ews:compiler}

Por los mismos motivos que se explicaban en el
capítulo~\ref{chap:designflow}, se hace necesaria una forma sencilla
de especificar las interfaces que implementa cada servicio, para que
un compilador pueda generar el código del autómata. Para ello creamos
un \fg{frontend} de \ac{WSDL} para el compilador \file{ipkc}, tal como
estaba previsto en su diseño \seesec{ipkc}.

La máquina de estados es un programa completo generado ad hoc. No se
utiliza \ac{FSM} puesto que los requisitos de memoria en este caso
superan las posibilidades de direccionamiento de la \ac{ISA}. En
cualquier caso, la posibilidad de utilizar el mismo modelo de
ejecución basado en máquina virtual que se propone para los \picos{}
resulta interesante también aquí. Para lograrlo se propone la
utilización de una versión de \ac{FSM} de 16 bits y algunas
instrucciones adicionales para manejar secuencias de bytes delimitadas
por banderas en lugar de por tamaños (que es lo habitual en los
protocolos inter-\ac{ORB} binarios). Por lo demás, no habría grandes
diferencias en el funcionamiento de un reconocedor de mensajes
\ac{SOAP} respecto al que se propone para \ac{IceP} o \ac{GIOP}.

En este caso, las entradas para el compilador son:

\begin{itemize}
\item La especificación de la interfaz del servicio en formato
  \ac{WSDL}. Los servicios proporcionados por el nodo deben estar
  descritos en este fichero.

\item La implementación del sirviente, es decir, las porciones de
  código que realmente realizan operaciones sobre el hardware del
  nodo. Idealmente pueden ser \fg{drivers} \seesec{drivers}.

\item La definición del servicio. El programador debe indicar qué
  servicios están disponibles en el nodo y las interfaces
  (\iface{portType}) que expondrá cada transductor. Este fichero
  utiliza el lenguaje IcePick \seesec{icepick}. El
  listado~\ref{code:EWS-icepick} muestra un ejemplo sencillo en el que
  se definen dos \ac{WS}: uno local y otro remoto. Indica también la
  ejecución de una invocación periódica en la que se envía el estado y
  el \fg{endpoint} del servicio.

\item La especificación de la interfaz del sirviente proporcionado por
  el programador. Se utiliza para ello el mismo lenguaje \ac{SIS} que
  se introdujo en la sección~\ref{sec:SIS}.
\end{itemize}

\begin{listing}[
  language = IcePick,
  caption =  {Ejemplo de IcePick para un \acs{WS}},
  label =    code:EWS-icepick]
uses "DUORW.wsdl";

object DUOIBoolRW_Service svc1;

local adapter myLocalEP {
  endpoint = "120.30.40.10:8060";
  objects = {svc1};
};

object DUOIBoolRW_Service svc2;

remote myRemoteEP {
  endpoint = "120.30.40.10:8070";
  objects = {svc2};
};

timer(5) {
  svc2.set(svc1.status(), svc1.endpoint);
}
\end{listing}

El compilador puede generar implementaciones de la máquina de estados
para varias plataformas por medio de \fg{backends}. Se ofrece soporte para
motas MICA2 e IRIS con TinyOS, y para microcontroladores ATmega128 y
8051 sin sistema operativo.


\section{Arquitectura de red}

En nuestro caso de ejemplo, un centro de control externo gestiona y
monitoriza edificios distribuidos en una gran extensión geográfica
(varias ciudades). Son frecuentes edificios con cientos o incluso
miles de luces de emergencia (aeropuertos, museos, estaciones, etc.).
Cada instalación está conectada a una red externa común
(p.ej. Internet) por medio de una o varias pasarelas
(figura~\ref{fig:EWS-network}).

\begin{figure}[ht]
\begin{center}
\includegraphics[width=0.9\textwidth]{EWS-network.pdf}
\caption{Topología de red para la gestión de luces de emergencia}
\label{fig:EWS-network}
\end{center}
\end{figure}


Cada pasarela realiza las siguientes tareas:

\begin{itemize}
\item Extrae mensajes \ac{SOAP} desde el protocolo de transporte de la
  red externa (\ac{TCP} en el ejemplo) y los encapsula en mensajes del
  protocolo de la red \ac{SAN}, que no tiene porqué estar basado en
  \ac{TCP/IP}. Y realiza el proceso inverso para los mensajes
  \ac{SOAP} salientes. Esta tarea se ilustra en la
  figura~\ref{fig:EWS-gateway}.
\item Debe implementar un mecanismo de correspondencia unívoca
  bidireccional entre direcciones de transporte (puerto \ac{TCP}) y el
  identificador de cada nodo. Éste es un aspecto crucial porque evita
  la necesidad de un procedimiento de configuración manual. Cuando se
  instala un nodo nuevo, la pasarela asigna un puerto \ac{TCP} para cada
  identificador de nodo.
\end{itemize}

Una consecuencia destacable es que este tipo de pasarelas no tiene
ningún conocimiento previo de la red, no están ligadas a una
aplicación concreta y, lo más importante, no realiza traducción de
protocolos sino encapsulación. Esto implica además que puede haber
varias pasarelas dando acceso a la misma red \ac{SAN} puesto que no
tienen estado. Lo único a tener en cuenta en ese caso es que la
dirección del nodo será con toda seguridad diferente dependiendo de la
pasarela que se utilice para contactar con el nodo. Eso puede
solventarse con un algoritmo que garantice que dado un identificador
de nodo se asocia siempre a la misma dirección externa. Por ejemplo,
si utilizamos las direcciones de nodo de una red con protocolo
\ac{XBow}, es posible hacer un \emph{mapping} directo entre
identificadores de nodo y puertos \ac{TCP} puesto que ambos son
números de 16 bits, y raramente una única \ac{SAN} necesitará más de
$2^{16}$ nodos.

\begin{figure}[ht]
\begin{center}
\includegraphics[width=\textwidth]{EWS-gateway.pdf}
\caption{Proceso de encapsulación de la pasarela}
\label{fig:EWS-gateway}
\end{center}
\end{figure}

La asociación establecida entre el puerto \ac{TCP} y el identificador
del nodo debe estar representada en el fichero \ac{WSDL}. El fichero
representa la interfaz del servicio para los usuarios y también sus
propiedades, por ejemplo la localización (el endpoint). Los dos datos
quedan plasmados en la etiqueta \id{soap:address} del fichero
\ac{WSDL}, tal como se muestra en el
listado~\ref{code:wsdl-definition}. La pasarela necesita conocer los
identificadores de nodo para generar el fichero \ac{WSDL}
correspondiente. Eso puede realizarse previamente si se conocen todos
los identificadores posibles o bien utilizar un
\ac{SDP}. Concretamente es posible usar \ac{ASDF}, aunque dentro de
la red \ac{SAN}.

Cuando se conecta un nodo a la red, éste envía un anunciamiento. Las
pasarelas interesadas en servir ese nodo hacia el exterior preguntan
al nodo cuál es su identificador y las interfaces que implementa. Para
ello se puede utilizar un \ac{MDOO} binario puesto que es un proceso
independiente y desacoplado del transporte de mensajes. La pasarela
construye el fichero \ac{WSDL} a partir de una plantilla que contiene
la interfaz del servicio. En la sección sobre implementación de
servicio incluimos información común sobre el entorno tal como la
localización del nodo en formato legible, el identificador del nodo,
etc.

\begin{listing}[
  language = XML,
  caption =  {Fragmento \acs{WSDL} para la localización de un nodo},
  label =    code:wsdl-definition]
[...]
<!-- Service to export -->
  <service name="ExitDoor_Service">
    <port name="IBoolRW_Port" binding="IBoolRW_Binding">
      <soap:address location="http://example.com:7890"/>
    </port>
  </service>
[...]
\end{listing}

Los servicios pueden ser localizados también por medio del protocolo
\ac{UDDI}. El proceso de publicación de un \ac{WS} asociado a
un nodo en un registro \ac{UDDI} es similar al de cualquier otro
\ac{WS}, es decir, la interfaz \ac{WS} como estructura t-model y la
implementación como servicio \ac{UDDI} business.

Además, es posible instalar otros servicios en la pasarela para
proporcionar otras prestaciones como registro, autorización, etc. La
pasarela puede generar los ficheros \ac{WSDL} a partir de un conjunto
de plantillas de cada una de las interfaces implementadas por los
nodos de la red. Pero hay tres alternativas posibles:

\begin{itemize}
\item Las plantillas pueden estar almacenadas en las pasarelas. Cuando
  se instala un nodo con una interfaz nueva es necesario añadir una
  nueva plantilla a cada pasarela. Esta situación introduce
  acoplamiento entre las pasarelas y las aplicaciones desplegadas en
  la \ac{SAN} pero no afecta a los nodos.
\item Cuando un nuevo nodo se anuncia, la pasarela puede solicitar la
  plantilla de las interfaces implementadas por el nodo, si es que las
  desconoce. Esto requiere almacenar la plantilla \ac{WSDL} en los
  nodos e implementar una operación para obtenerla, lo que implica más
  memoria \ac{ROM} o Flash en los nodos, aunque permite pasarelas más
  sencillas y completamente independientes.
\item Una tercera alternativa es colocar las plantillas en un servidor
  remoto de modo que las pasarelas las puedan solicitar bajo
  demanda. No implica mayores necesidades de memoria en los nodos ni
  acoplamiento para las pasarelas, pero requiere de un \fg{host} remoto
  conocido por todas las pasarelas.
\end{itemize}


\section{Prototipos \acs{EWS}}
\label{sec:protos:ews}

Utilizando las propuestas descritas, hemos desarrollado un conjunto de
prototipos sobre las plataformas típicas en \ac{WSN}: el Atmel
ATmega128 con el sistema operativo TinyOS. También hemos realizado
algunos prototipos sobre dispositivos sin sistema operativo.

Comparamos el tamaño de distintas implementaciones de \ac{WS}. En un
primer paso, implementamos un servicio básico \emph{get/set}
\seesec{ews:bottomup}, usando dos bibliotecas ligeras de propósito
general: \file{csoap}~\cite{csoap} y
\file{libxml-rpc}~\cite{libxmlrpc}. Después implementamos el mismo
servicio utilizando nuestra estrategia en tres plataformas diferentes:
\ac{PC}, MICA2 con TinyOS y un microcontrolador AVR sin sistema
operativo. El tamaño de los binarios resultantes se muestra en la
tabla~\ref{tab:ews-sizes}. Nótese que:

\begin{itemize}
\item Todos los prototipos x86  están enlazados
  estáticamente y se ejecutan en un \ac{PC} convencional con \ac{SO}
  Debian \ac{GNU}/Linux. El tamaño del \ac{SO} y sus dependencias no
  se han considerado.
\item Los prototipos x86 de \acs{EWS} son programas C puros con el
  soporte estándar de sockets.
\item Los prototipos con MICA2 y AVR incluyen todos los componentes
  requeridos. Los datos que aparecen en la tabla se refieren al
  fichero que instalamos en el dispositivo. El prototipo AVR no
  utiliza \ac{SO}, sólo un sencillo ejecutivo cíclico, mucho más
  pequeño que TinyOS, aunque obviamente a costa de sus prestaciones.
\end{itemize}

\begin{table}[htb]
  \centering
  \input{tables/ews-sizes.tex}
  \caption[Comparativa de tamaños de un \acs{WS} para un
  servicio get/set  básico]
  {Comparativa de tamaños (en bytes) de un \acs{WS} para un
    servicio get/set  básico}
  \label{tab:ews-sizes}
\end{table}

El código fuente de todos los prototipos, la implementación de
referencia y otros ficheros necesarios están disponibles en la página
web de \ac{EWS}~\cite{EWS-web}.


\section{Conclusiones}

\ac{EWS} es una propuesta para implementar \ac{WS} directamente en los
nodos de una \ac{SAN} o \ac{WSN}. Hasta donde sabemos, no existe una
estrategia previa que permita algo semejante en dispositivos con
capacidad de cómputo tan limitada ya que lo habitual es exponer los
\ac{WS} únicamente desde la pasarela.

Empotrar un \ac{WS} directamente en el nodo \ac{SAN} permite construir
pasarelas genéricas independientes de la aplicación. De ese modo, se
proporciona un despliegue físico de dispositivos con procedimientos de
configuración mínimos.

Se han implementado algunos prototipos con transporte \ac{SOAP} y
\ac{XML-RPC} para demostrar la viabilidad de la solución propuesta
\seesec{protos:ews}. En estos momentos, nuestro objetivo es
mejorar el soporte para la generación y prueba de máquinas de estados
mediante el compilador que permite la construcción de nodos listos
para usar. También nos parece muy interesante diseñar una nueva
\ac{FSM} que permita unificar el desarrollo y despliegue de \picos{} y
\ac{WS} en una misma red e, incluso, sobre los mismos nodos.


% Local variables:
%   ispell-local-dictionary: "castellano8"
%   TeX-master: "main.tex"
% End:
