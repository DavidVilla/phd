% -*- coding: utf-8 -*-

\chapter{General Inter-ORB Protocol}
\hyperlabel{chap:GIOP}
\minitocbox


Este anexo es un resumen del capítulo \fg{General Inter-\ac{ORB}
  Protocol} del documento central de la especificación
\ac{CORBA}~\cite{CORBA_spec}. Una parte importante de este capítulo es
una traducción más o menos libre del original, aplicando como criterio
la claridad y la relación con el objetivo que nos ocupa. Se han
omitido las secciones que tratan características no soportadas por
picoCORBA, tales como serialización de tipos complejos, localización
de objetos o \ac{GIOP} bidireccional.


\section{Visión general de \acs{GIOP}}

Después de las primeras versiones del estándar de \ac{CORBA} se observó la
necesidad de definir un protocolo común para comunicar varios \acp{ORB}.

A partir de \ac{CORBA} 2.0 se definió la arquitectura de
interoperabilidad entre \acp{ORB} de distintos fabricantes sobre el
protocolo \ac{GIOP}.  Sin embargo, se mantuvo la idea de poder
utilizar, además de \ac{GIOP}, cualquier otro protocolo propio del
fabricante (\acs{ESIOP}). Estas dos ideas se han respetado hasta el momento
y son la base que permite la interoperabilidad entre \acp{ORB} de
diferentes fabricantes.

\ac{GIOP} puede mapearse sobre cualquier protocolo de transporte
orientado a conexión que cumpla unos mínimos requisitos.  \ac{CORBA}
define un \fg{mapping} de \ac{GIOP} específico para conexiones
\ac{TCP/IP} llamado \acx{IIOP} cuyo diagrama de capas aparece en la
figura~\ref{fig:capasCORBA}.

\begin{figure}[h!]
\begin{center}
\begin{tabular}{c} \hline
Objetos de la aplicación \\
\tabcolorrow
\ac{ORB}    \\
\ac{IIOP}   \\
\tabcolorrow
\ac{TCP}    \\
\ac{IP}     \\
\tabcolorrow
Ethernet    \\
Medio físico\\
\hline
\end{tabular}
\caption{Diagrama de capas de \acs{CORBA}}
\label{fig:capasCORBA}
\end{center}
\end{figure}


Los objetivos más importantes que se tuvieron en cuenta en el diseño
de \ac{GIOP} fueron:
\begin{definitionlist}
\item[Disponibilidad] \ac{GIOP} está basado en el mecanismo de
  transporte más flexible y usado que existe (\ac{TCP/IP}), y define un
  protocolo mínimo adicional para transmitir peticiones \ac{CORBA} entre
  \acp{ORB}.
\item[Simplicidad]  \ac{GIOP} está pensado para ser lo más simple posible
  siempre que cumpla los demás objetivos.  La simplicidad asegura
  variedad de implementaciones independientes y compatibles.
\item[Escalabilidad]  \ac{GIOP}/\ac{IIOP} debería soportar \acp{ORB} y puentes
  (\emph{bridges}) de \acp{ORB} dado el tamaño actual y futuro de Internet.
\item[Bajo coste]  Añadir soporte para \ac{GIOP} a un \ac{ORB} nuevo o existente
  debería requerir poco esfuerzo en desarrollo.  Además, el coste
  requerido para soportar \ac{IIOP} en un \ac{ORB} en exploración debería ser
  mínimo.
\item[Generalidad]  El formato de los mensajes \ac{GIOP} está diseñado para
  ser usado con cualquier protocolo de transporte orientado a
  conexión.
\item[Neutralidad arquitectural]  La especificación de \ac{GIOP} hace
  suposiciones mínimas sobre la arquitectura de agentes que
  soporta.  \ac{GIOP} trata a los \acp{ORB} como entidades opacas con
  arquitecturas desconocidas.
\end{definitionlist}


La especificación de \ac{GIOP} consta de los siguientes elementos:
\begin{itemize}
\item Definición de \acx{CDR}.  \ac{CDR} es un \fg{mapping} de los tipos
  de datos de \ac{IDL} a una representación bicanónica de bajo nivel
  para transferencias entre \acp{ORB} o entre puentes Inter-\ac{ORB}
  (agentes).
\item Formatos de mensajes \ac{GIOP}.  Los agentes intercambian
  mensajes \ac{GIOP} para conseguir peticiones a objetos, localización
  de implementaciones y gestión de canales de comunicación.
\item Consideraciones sobre el transporte \ac{GIOP}. La especificación
  \ac{GIOP} describe ciertas condiciones generales que conciernen a
  cualquier protocolo de transporte que puede ser usado para
  transferir mensajes \ac{GIOP}.  También describe cómo gestionar las
  conexiones y las restricciones en la ordenación de mensajes.
\end{itemize}

\subsection{Representación común de datos (\acs{CDR})}

\ac{CDR} tiene las siguientes características:

\begin{definitionlist}
\item[Ordenación de bytes variable] Las máquinas con una ordenación de
  bytes (\fg{byte order}) común pueden intercambiar datos sin tener
  que realizar modificaciones en el ordenamiento.  Cuando la
  comunicación se realiza entre máquinas con ordenación diferente, el
  emisor determina la ordenación de bytes en el mensaje, y el receptor
  es responsable de realizar el intercambio para que corresponda con
  su ordenamiento.  Cada mensaje \ac{GIOP} (y encapsulación \ac{CDR})
  contiene un \fg{flag} que indica el ordenamiento.
\item[Tipos primitivos alineados] Los tipos de datos
  primitivos de \ac{IDL} se alinean a sus límites naturales dentro de
  los mensajes, permitiendo que los datos se puedan manejar de forma
  más eficiente por las arquitecturas que fuerzan el alineamiento de
  datos en memoria.
\item[\fg{Mapping} completo de \ac{IDL}]  \ac{CDR} describe la representación
  de todos los tipos de datos de \ac{IDL}, incluyendo pseudo-objetos como
  \emph{TypeCodes}.
\end{definitionlist}

\subsection{Mensajes \acs{GIOP}}
\ac{GIOP} especifica el formato de los mensajes que se intercambian los
\acp{ORB}.  El formato de los mensajes \ac{GIOP} tiene las siguientes
características:
\begin{definitionlist}
\item[Mensajes simples]  Con pocos mensajes, \ac{GIOP} soporta la
  funcionalidad completa de \ac{CORBA} entre \acp{ORB}, con capacidades
  extendidas que permiten servicio de localización de objetos,
  migración dinámica, gestión eficiente de comunicación de recursos.
  La semántica de \ac{GIOP} no requiere negociación.  En muchos casos, los
  clientes pueden invocar operaciones inmediatamente después de
  establecerse la conexión.
\item[Localización dinámica de objetos]  Muchas arquitecturas
  de \ac{ORB} permiten que la implementación de un objeto sea activada
  desde un lugar diferente durante su ciclo de vida, y permite a los
  objetos migrar dinámicamente.  Los mensajes \ac{GIOP} dan soporte
  para la localización y migración de objetos. No se requiere
  que los \acp{ORB} implementen dichos mecanismos cuando sea innecesario
  o inapropiado.
\item[Soporte completo para \ac{CORBA}]  Los mensajes
  \ac{GIOP} posibilitan directamente todas las funciones y
  comportamientos requeridos por \ac{CORBA}, incluyendo manipulación
  de excepciones, contextos de operación e invocaciones sobre
  referencias a objetos remotos.
\end{definitionlist}

\ac{GIOP} también permite el paso de contextos propios de los
servicios, tales como el contexto del servicio estándar de
transacción.  Este mecanismo está diseñado para soportar cualquier
servicio que requiera información de contexto que tenga que ser
enviada implícitamente en las peticiones.


\subsection{Transferencia de mensajes \acs{GIOP}}
La especificación \ac{GIOP} está diseñada para operar sobre cualquier
protocolo de transporte que cumpla unas mínimas condiciones
\seesec{transporteGIOP}.  \ac{GIOP} utiliza conexión de varias
maneras:

\begin{definitionlist}
\item[Uso asimétrico de conexión]  \ac{GIOP} define dos roles
  diferentes con respecto a las conexiones: cliente y servidor.  El
  cliente origina la conexión y envía petición a los objetos a través
  de esa conexión.  El servidor recibe las peticiones y envía
  respuestas.
\item[Multiplexación de peticiones]  Si es necesario, varios clientes de
  un \ac{ORB} pueden compartir una conexión para enviar peticiones a un \ac{ORB}
  o servidor concreto.  Cada petición únicamente identifica al objeto
  destino.  Varias peticiones independientes para uno o varios objetos
  se pueden enviar sobre la misma conexión.
\item[Peticiones solapadas]  En general, la ordenación de mensajes
  \ac{GIOP} es mínima.  \ac{GIOP} está diseñado para permitir el solapamiento de
  peticiones asíncronas.  Los identificadores únicos permiten una
  adecuada correlación entre peticiones y respuestas.  Las
  implementaciones son libres de imponer mecanismos de ordenación
  internos si las arquitecturas de sus \acp{ORB} lo requieren.
\item[Gestión de conexiones]  \ac{GIOP} define mensajes para cancelación y
  finalización controlada de las conexiones.  Estas características
  permiten a los \acp{ORB} reutilizar los recursos no ocupados.
\end{definitionlist}


\section{Sintaxis de transferencia de \acs{CDR}}
La sintaxis de transferencia \ac{CDR} es el formato en que \ac{GIOP} representa
los tipos de datos \ac{IDL} en un flujo de octetos.

Un flujo de octetos es una notación abstracta que corresponde con un
\fg{buffer} de memoria que debe ser transmitida a otro proceso con un
mecanismo de comunicación entre procesos (\ac{IPC}) o transporte de red.  Un
flujo de octetos es arbitrariamente largo (pero finito), formado por
valores de 8 bits (octetos) y con un comienzo bien definido.  Cada
flujo puede ser indexado en una posición de 0 a n-1, siendo \emph{n}
el número de octetos del flujo.  La posición que ocupa un octeto se
llama índice; estos índices se usan para alinear los datos \ac{IDL} dentro
del flujo.

\ac{GIOP} define dos tipos de flujos de octetos, mensajes y
encapsulaciones.  Los mensajes son unidades básicas de información
\ac{GIOP}, que se describe en el
sección~\ref{sec:GIOP:formatoMensajes}.  Las encapsulaciones son
tramas en las que las estructuras de datos \ac{IDL} se pueden
serializar (\fg{marshall}) independientemente de cualquier contexto
del mensaje.  Una vez que una estructura de datos está encapsulada, el
flujo de octetos puede representarse como un tipo \ac{IDL}
\type{sequence<octet>} opaco, que se puede serializar en otro
mensaje o encapsulación.  La encapsulación permite constantes
complejas (como los \type{TypeCode}); esto también permite que ciertos
componentes del mensaje puedan usarse sin tener que realizar una
deserialización completa.

\subsection{Tipos primitivos}
Los tipos primitivos están definidos en orden \fg{big-endian}
y en \fg{little-endian}; tanto los mensajes como las encapsulaciones
disponen de un \fg{flag} en su cabecera que indica el tipo de ordenamiento
que usan.  Las encapsulaciones en el interior de los mensajes pueden
tener un ordenamiento distinto al propio mensaje.  Los tipos de datos
primitivos se codifican en múltiplos de octeto.


\subsubsection{Alineamiento}
Para trasladar los tipos primitivos a las tramas de octetos se definen
funciones específicas para cada uno de los tipos; \ac{CDR} requiere que
todos los tipos primitivos estén alineados a sus límites naturales.  El
alineamiento de los tipos primitivos es igual al tamaño del tipo en
bytes.  Un tipo primitivo de tamaño \emph{n} debe comenzar en una
posición de la trama múltiplo de \emph{n}; en \ac{CDR}, \emph{n} puede ser
1, 2, 4 u 8.


Cuando sea necesario, pueden aparecer huecos (\fg{alignment gap}) antes
de la representación de los datos.  El valor de los octetos que
conforman los huecos no está definido.  En la
tabla~\ref{tab:alineamiento} se muestra la alineación en octetos que
deben tener los tipos primitivos.

\begin{table}[h!]
  \begin{center}
    \input{tables/CDRalignement.tex}
    \caption{Alineación requerida para los tipos primitivos de \acs{IDL}}
    \label{tab:alineamiento}
  \end{center}
\end{table}


El alineamiento se define respecto al comienzo de la trama de
octetos.  El primer octeto de la trama tiene índice cero; todos los
tipos de datos se almacenan comenzando por este índice.  La trama
de octetos empieza con una cabecera de mensaje \ac{GIOP}
\seesec{GIOP:head} o con una encapsulación, incluso si está
anidada en otra encapsulación.

\subsubsection{Tipos enteros}
En la tabla~\ref{tab:CDR-integer} se muestra la ordenación de bytes,
tanto en \fg{big-endian} como en \fg{little-endian}, para los
enteros definidos en \ac{IDL}, entre los que se encuentran los siguientes:

\begin{itemize}
\item \type{short}
\item \type{unsigned short}
\item \type{long}
\item \type{unsigned long}
\item \type{long long}
\item \type{unsigned long long}
\end{itemize}

\input{tables/CDRinteger.tex}


\subsubsection{Tipos en punto flotante}
Un número en punto flotante está compuesto del bit de signo, el
exponente y la parte fraccionaria de la mantisa, tal y como se define
en el estándar \ac{IEEE} para los números en punto
flotante~\cite{IEEE-float}.  La tabla~\ref{tab:CDR-float}
representa los diferentes componentes para números en punto flotante:
el bit de signo (\emph{s}), el exponente (\emph{e}) y la parte
fraccionaria de la mantisa (\emph{f}).  El bit de signo puede tomar
valores 0 ó 1, representando positivo y negativo respectivamente.

\input{tables/CDRfloat.tex}


\subsubsection{Octetos}
El octeto es un valor de 8 bits no interpretado para el cual está garantizado
que su contenido no se convertirá ni modificará durante la transmisión.  A
efectos de especificación se puede considerar a los octetos como
enteros sin signo de 8 bits.

\subsubsection{Booleanos}
Están codificados como octetos simples, donde TRUE (cierto) es el valor 1, y
FALSE (falso) es el valor 0.

\subsubsection{Caracteres}
Un carácter \ac{IDL} se representa como un octeto simple, la tabla de
códigos (\emph{code set}) que se usa en la transmisión de caracteres
entre los \acp{ORB} de un servidor y un cliente se determina en el
propio proceso de transmisión.  En caso de codificación
multi-byte\footnote{Se utilizan varios bytes para codificar cada
  carácter.}, una instancia del tipo carácter (\type{char}) sólo
puede contener uno de los valores del multi-byte.

La sintaxis de transferencia para caracteres anchos (\type{wide char})
depende de la versión de \ac{GIOP} (véase \S 13.7, \emph{Code Set
  Conversion} de \cite{CORBA_spec}).

\subsection{Tipos compuestos}

Los tipos de datos compuestos se construyen a partir de los tipos
primitivos usando medios definidos en el lenguaje \ac{IDL}.

\subsubsection{Alineamiento}
Los tipos compuestos no tienen restricciones de alineamiento aparte de
las de sus componentes primitivos.  \ac{GIOP} asume que los agentes
construyen los tipos de datos estructurados copiando datos primitivos
entre el buffer serializado y la estructura de datos en memoria para
la implementación del mapping del lenguaje concreto.

\subsubsection{Estructuras (\fg{struct})}
Los elementos de una estructura se codifican en el orden de su
declaración \ac{IDL}.  Cada componente se codifica según su propio tipo.


\subsubsection{Union}
Se codifican como una etiqueta discriminante (del tipo especificado en
la declaración), seguido por la representación de miembros
correspondientes a la etiqueta, codificados según su propio tipo.

\subsubsection{Array}
Se codifican como un vector de elementos en secuencia.  Como la
longitud del array es fija, no se incluyen valores de longitud.  Cada
elemento se codifica según su tipo.  En arrays multidimensionales, los
elementos se ordenan de modo que el índice de la primera dimensión varía
más despacio.

\subsubsection{Secuencias (\emph{Sequence})}
Se codifican como un valor \type{unsigned long} que indica el
tamaño seguido por los elementos de la secuencia.  Dichos elementos se
codifican según su tipo.

\subsubsection{Enumeraciones (\emph{enum})}
Los valores de las enumeraciones se almacenan como \type{unsigned
  long}.  El valor numérico asociado a cada identificador se determina
por el orden de éstos en la definición del tipo comenzando con cero;
el resto reciben valores en orden ascendente de izquierda a derecha.

\subsubsection{Cadenas}
Una cadena (\type{string}) se codifica como \type{unsigned long} que
indica la longitud en octetos, seguido por el valor de los caracteres
que forman la cadena ya sea en formato de caracteres simples o
multi-byte; la representación es equivalente a una secuencia de
octetos.  La cadena incluye un carácter simple nulo que indica el
fin; la longitud de la cadena incluye dicho carácter, de modo que el
tamaño de una cadena vacía es 1.

\subsubsection{Tipos decimales en punto fijo}
El tipo \ac{IDL} \type{fixed} no tiene restricciones de alineación.
Cada octeto contiene dos dígitos decimales.  Cada
\fg{nibble}\footnote{Semi-octeto.} codifica un dígito en
hexadecimal.  Si el tipo fijo tiene un número par de dígitos
decimales, la representación empieza por el dígito más significativo;
en otro caso, el primer \fg{nibble} es un cero y el segundo es el dígito
más significativo del número. La representación del signo es el último
\fg{nibble}: \id{0xD} si es negativo y \id{0xC} si es positivo o cero.

El número de dígitos debe ser igual al número de dígitos
significativos especificado en la definición \ac{IDL}, con la excepción del
\fg{nibble} añadido en el caso de valores con un número impar de dígitos
significativos.


\subsection{Encapsulación}
Los tipos de datos \ac{IDL} se pueden serializar en encapsulaciones de
tramas de octetos.  La trama de octetos se representa con el tipo \ac{IDL}
\code{sequence<octet>}, que puede ser incluida en un mensaje \ac{GIOP} o
anidada en otra encapsulación.

\acs{GIOP} e \acs{IIOP} utilizan encapsulaciones explícitamente en tres sitios:
\begin{itemize}
\item TypeCodes.
\item Perfiles \acs{IIOP} en el interior de \acs{IOR}.
\item Contextos específicos del servicio.
\end{itemize}

Cuando se encapsulan tipos \ac{IDL}, el primer octeto de la trama (índice
0) contiene un valor booleano que indica el ordenamiento de los datos
encapsulados.  Si el valor es FALSE (0), los datos encapsulados están
codificados en \fg{big-endian}; si es TRUE (1), se codifican en
\fg{little-endian}, es el mismo criterio que en las cabeceras
\ac{GIOP} \seesec{GIOP:head}.  Este valor no es parte de los
datos encapsulados, pero es parte del flujo de octetos incluidos en la
encapsulación.  A continuación del \fg{flag} de ordenamiento, los datos a
encapsular se serializan en el buffer tal como indican las reglas de
\ac{CDR}.  Los datos serializados se alinean con relación al comienzo del
flujo de octetos (el primer octeto ocupado por el \fg{flag} de
ordenamiento).

Cuando la encapsulación se codifica con el tipo
\type{sequence<octet>} para su serialización, se coloca delante un
\type{unsigned long} cuyo valor es el tamaño de la secuencia.  El
valor de la longitud no es parte de la encapsulación y no afecta al
alineamiento de los datos.  Esto garantiza un alineamiento de 4 bytes
al comienzo de todos los datos en mensajes \ac{GIOP} y encapsulaciones
anidadas.


\section{Formato de los mensajes \acs{GIOP}}
\label{sec:GIOP:formatoMensajes}
\ac{GIOP} está restringido al modelo cliente/servidor en lo referente
a inicialización y recepción de mensajes.  Para las versiones 1.0 y 1.1
de \ac{GIOP}, un cliente es un agente que abre una conexión y envía
peticiones; un servidor es un agente que acepta conexiones y recibe
dichas peticiones.  Sin embargo, cuando se utiliza \ac{GIOP}
bidireccional en el protocolo \ac{GIOP} versiones 1.2 y 1.3,
cualquiera de los interlocutores puede originar mensajes tal como se
especifica en la sección \emph{Bi-Directional GIOP} en
~\cite{CORBA_spec}.

El cuadro~\ref{tab:GIOP-messages} resume todos los mensajes
\ac{GIOP}, indicando su nombre, el emisor (cliente, servidor o ambos)
y el valor usado para identificar el mensaje en la cabecera del
mensaje \ac{GIOP}.

\begin{table}[htbp]
  \centering
  \input{tables/GIOPmsg.tex}
  \caption{\acs{GIOP}: Tipos de mensajes}
  \label{tab:GIOP-messages}
\end{table}

En esta sección se describe sólo el formato y funcionamiento de
\ac{GIOP} versión 1.0, por ser la primera y más simple de las
definidas hasta el momento por \ac{OMG}.  Las versiones posteriores: 1.1,
1.2 y 1.3 han incorporado mejoras y nuevas características al
protocolo.  Aún así, la esencia del mismo no ha cambiado y, por tanto,
\acs{GIOP}~1.0 es una referencia más que suficiente para comprender
perfectamente la finalidad del protocolo.  Para una descripción
detallada de versiones más recientes de \ac{GIOP},
consultar~\cite{CORBA_spec}.  El formato de los mensajes se describe
en \ac{IDL} conforme a la especificación original.


\subsection{Cabecera \acs{GIOP}}
\label{sec:GIOP:head}
Todos los mensajes \ac{GIOP} comienzan con una cabecera de tamaño fijo
descrita en el listado~\ref{code:GIOP:head}.

\begin{listing}[
  language = IDL,
  caption =  {Formato de la cabecera \acs{GIOP}},
  label =    code:GIOP:head]
module GIOP {
  struct Version {
    octet major;
    octet minor;
  };

  enum MsgType_1_0 {
    Request, Reply, CancelRequest,
    LocateRequest, LocateReply,
    CloseConnection, MessageError
  };

  struct MessageHeader_1_0 {
    char             magic[4];
    Version          GIOP_version;
    boolean          byte_order;
    octet            message_type;
    unsigned long    message_size;
  };
};
\end{listing}


La cabecera identifica el mensaje \ac{GIOP} y su ordenación de bytes
(\fg{byte-order}).  La cabecera es independiente de la ordenación
excepto para los siguientes campos:

\begin{description}
\item[\id{magic}] Identifica a los mensajes \ac{GIOP}.  El valor de
  este miembro es constante y corresponde con los caracteres «GIOP»,
  en mayúsculas y codificados en \acs{ISO} Latin-1 (8859.1).
\item[\id{GIOP\_version}] Es el número de versión del protocolo
  \ac{GIOP} usado en el mensaje.  Éste se aplica a los
  elementos independientes del transporte, es decir, \ac{CDR} y el formato
  de los mensajes.  Es diferente de la versión de \ac{IIOP} aunque
  tenga la misma estructura.

  Una implementación que soporte \ac{GIOP}~1.n (con $n > 0$) debe también
  poder procesar mensajes \ac{GIOP} de versiones anteriores.  Un servidor
  que recibe una petición de una versión \ac{GIOP} mayor de la que soporta
  debería responder con un mensaje de error con el número de versión
  mayor que soporta y, después, cerrar la conexión.

  Ningún cliente debería enviar un mensaje con una versión mayor que
  la publicada por el servidor en el perfil \ac{IIOP}
  de su \ac{IOR}.
\item[\id{byte\_order}] Indica el ordenamiento del mensaje (incluyendo el
  campo \id{message\_size}). Un valor 0 indica \fg{big-endian} y
  un valor 1 indica \fg{little-endian}.
\item[\id{message\_type}] Indica el tipo de mensaje de acuerdo con el
  cuadro~\ref{tab:GIOP-messages}.  Los valores posibles corresponden con el tipo
  \textbf{MsgType}.
\item[\id{message\_size}] Contiene el número de octetos del mensaje (sin
  incluir la cabecera) codificados con el ordenamiento indicado por el
  campo \id{byte\_order}. Indica el tamaño del cuerpo del mensaje, este
  valor incluye los octetos necesarios para los huecos de alineamiento
  (\fg{alignment gap}), los parámetros de la petición o respuesta y
  los octetos de relleno al final del mensaje de modo que termine en
  un límite de 8 bytes.
\end{description}


\subsection{Mensaje de petición (\fg{Request})}
\label{sec:GIOP:Request}
Los mensajes de petición codifican las invocaciones a objetos
\ac{CORBA}, incluyendo acceso a los atributos y operaciones de
\iface{CORBA::Object} como \function{get\_interface()} y
\function{get\_implementation()}.  Las peticiones fluyen del cliente
hacia el servidor.

Los mensajes de petición tienen tres elementos, codificados en este
orden:
\begin{itemize}
\item Una cabecera de mensaje \ac{GIOP}.
\item Una cabecera de petición.
\item El cuerpo de la petición.
\end{itemize}

\subsubsection{Cabecera de petición}
La cabecera de petición está definida como sigue:

\begin{listing}[language=IDL]
module GIOP {
  struct RequestHeader_1_0 {
    IOP::ServiceContextList   service_context;
    unsigned long             request_id;
    boolean                   response_expected;
    sequence<otet>            object_key;
    string                    operation;
    CORBA::OctetSeq           requesting_principal;
  }
};
\end{listing}


Los miembros tienen los siguientes significados:
\begin{description}
\item[\id{service\_context}] Contiene datos del \ac{ORB} que se pasan
  del cliente al servidor tal como se explica en \emph{Object Service
    Context} en~\cite{CORBA_spec}.
\item[\id{request\_id}] Se usa para asociar los mensajes de
  respuesta con los de petición (incluyendo los mensajes
  \textbf{LocateRequest}).  El cliente peticionario es el responsable
  de generar valores de forma que no haya ningún tipo de ambigüedad;
  específicamente, un cliente no debería reutilizar los mismos valores
  durante una conexión si:

\begin{enumerate}
\item La última petición que contenía el identificador aún está
  pendiente de respuesta.
\item La última petición que contenía el identificador fue cancelada
  y no se recibió respuesta.
\end{enumerate}

\item[\id{response\_expected}] Indica que el cliente espera recibir
  una respuesta para la petición si su valor es \id{TRUE}.
\item[\id{object\_key}] Identifica al objeto al que va dirigida la
  petición.  Corresponde con el valor del campo \id{object\_key} del perfil
  \ac{GIOP} presente en la \ac{IOR} para ese objeto.  El cliente no debe
  modificar ni interpretar nunca este valor.
\item[\id{operation}] Es un identificador \ac{IDL} en el contexto
  de la interfaz que implementa el objeto y que identifica el método
  que el cliente desea invocar.  En el caso de accesores, los nombres
  de las operaciones son \function{\_get\_<attribute>} y
  \function{\_set\_<attribute>}.  El valor de la operación debe
  corresponder con un nombre o atributo usado en la declaración de la
  interfaz \ac{IDL}.

  Las operaciones definidas para \iface{CORBA::Object} son:
  \begin{itemize}
  \item \_interface.
  \item \_is\_a.
  \item \_non\_existent.
  \item \_domain\_managers.
  \item \_component.
  \end{itemize}
\item[\id{requesting\_principal}] Es un campo que se utilizaba en
  implementaciones del \acs{BOA}, predecesor del \ac{POA} y que hoy
  está en desuso.
\end{description}

\subsubsection{Cuerpo de la petición}
En \ac{GIOP}~1.0 y 1.1, el cuerpo de la petición se serializa en la
encapsulación \ac{CDR} inmediatamente después de la cabecera de
petición.  Los datos en el cuerpo de la petición incluyen los
siguientes elementos en este orden:

\begin{itemize}
\item Todos los parámetros de entrada (\textbf{in}) y entrada/salida
  (\textbf{inout}), en el orden en que se especifiquen en la
  definición de la operación en \ac{IDL}, de izquierda a derecha.
\item Un pseudo-objeto opcional \textbf{Context}.  Este elemento está
  presente sólo si la definición de la operación en \ac{IDL} incluye
  una expresión de contexto y únicamente aparecen los miembros
  definidos en la expresión \ac{IDL}.
\end{itemize}


\subsection{Mensaje de respuesta (\fg{Reply})}
Los mensajes de respuesta se devuelven al emisor del mensaje de
petición sólo si el \fg{flag} de \id{response\_expected}
tiene un valor \id{TRUE}.  El mensaje de respuesta incluye los parámetros
de salida, los de entrada/salida y el valor de retorno de la
operación.  En \ac{GIOP}~1.0 y 1.1 los mensajes de respuesta fluyen sólo
del servidor hacia el cliente.

Los mensajes de respuesta tienen tres elementos, codificados en este
orden:
\begin{itemize}
\item Una cabecera \ac{GIOP}.
\item Una cabecera de respuesta.
\item El cuerpo de la respuesta.
\end{itemize}

\subsubsection{Cabecera de respuesta}
La cabecera de respuesta está definida como sigue:

\begin{listing}[language=IDL]
module GIOP {
  enum ReplyStatusType_1_0 {
    NO_EXCEPTION,
    USER_EXCEPTION,
    SYSTEM_EXCEPTION,
    LOCATION_FORWARD
  };

  struct ReplyHeader_1_0 {
    IOP::ServiceContextList   service_context;
    unsigned long             request_id;
    ReplyStatusType_1_0       reply_status;
  };
};
\end{listing}


Siendo:
\begin{description}
\item[\id{request\_id}] Se usa para asociar las respuestas a las
  peticiones.  Contiene el valor del campo del mismo nombre en la
  petición correspondiente.
\item[\id{reply\_status}] Indica el estado de la petición asociada, y
  también determina parte del contenido del cuerpo del mensaje de
  respuesta.  Si no ocurren excepciones y la operación se completa
  satisfactoriamente, el valor es \id{NO\_EXCEPTION} y el cuerpo
  contiene los valores de retorno.  En otro caso, el cuerpo puede:
  \begin{itemize}
  \item Contener una excepción,
  \item Indicar al cliente que redirija la petición a un objeto que se
    encuentra en otra localización, o
  \item Pedir al cliente que indique más información de direccionamiento.
  \end{itemize}
\item[\id{service\_context}] Es equivalente al campo del mismo nombre
  presente en la cabecera de petición.
\end{description}

No se incluyen octetos de relleno (\fg{padding}) cuando el cuerpo
del mensaje de respuesta es vacío.

\subsubsection{Cuerpo del mensaje de respuesta}
En \ac{GIOP}~1.0 y 1.1, el cuerpo del mensaje de respuesta está serializado
en una encapsulación \ac{CDR} que aparece inmediatamente a continuación de
la cabecera de respuesta.  El contenido del mensaje de respuesta está
determinado por el valor del campo \id{reply\_status}.  El cuerpo
puede ser de los siguientes tipos dependiendo del valor del campo
\id{reply\_status}:

\begin{itemize}
\item Si es \id{NO\_EXCEPTION}, en el
  cuerpo se codifica primero el valor de retorno de la operación, si
  lo hubiere, después los parámetros entrada/salida y de salida en el
  orden en el que aparecen en la declaración de la operación en \ac{IDL},
  de izquierda a derecha.
\item Si es \id{USER\_EXCEPTION} o \id{SYSTEM\_EXCEPTION}, el cuerpo
  contiene la excepción levantada por la operación.  Sólo se pueden
  levantar las excepciones de usuario que aparezcan en la definición
  de operación del \ac{IDL}.

  Cuando el valor del campo \id{reply\_status} es
  \id{SYSTEM\_EXCEPTION}, el cuerpo de la respuesta tiene la
  siguiente estructura:

  \begin{listing}[language=IDL]
module GIOP {
  struct SystemExceptionReplyBody {
    string          exception_id;
    unsigned long   minor_code_value;
    unsigned long   completion_status;
  };
};
  \end{listing}

\item Si es \id{LOCATION\_FORWARD}, el cuerpo contiene una
  referencia a objeto (\ac{IOR}).  El \ac{ORB} cliente es responsable de
  reenviar la petición original al objeto asociado a dicha
  referencia.  Este reenvío es transparente para el programa cliente
  que hace la petición.
\end{itemize}

\subsection{Mensaje de cancelación de petición (\emph{CancelRequest})}
\label{sec:GIOP:CancelRequest}
En \ac{GIOP}~1.0 y 1.1 son mensajes enviados por el cliente al servidor
para notificar que el cliente no seguirá esperando la respuesta para
un mensaje de petición pendiente.

El mensaje de cancelación de petición tiene dos elementos:
\begin{itemize}
\item Una cabecera de mensaje \ac{GIOP}.
\item Una cabecera de mensaje de cancelación de petición.
\end{itemize}

\subsubsection{Cabecera de cancelación de petición}
Está definida del siguiente modo:

\begin{listing}[language=IDL]
module GIOP {
  struct CancelRequestHeader {
    unsigned long       request_id;
  };
};
\end{listing}

\id{request\_id} identifica el mensaje de petición, o petición
de localización (\emph{LocateRequest}), al que se aplica la
cancelación.  Este valor es el mismo que el especificado en el campo
del mismo nombre en el mensaje de petición original.

Cuando un cliente emite un mensaje de cancelación de petición sólo
supone un aviso.  El servidor puede hacer caso omiso y enviar el
mensaje de respuesta de todos modos.  El cliente no debería hacer
suposiciones sobre si llegará o no la respuesta una vez enviado el
mensaje de cancelación de petición.


\subsection{Mensaje de petición de localización (\emph{LocateRequest})}
Estos mensajes pueden ser enviados por un cliente a un servidor para
determinar las siguientes características sobre el objeto:

\begin{itemize}
\item Si el servidor es capaz de dirigir peticiones hacia la
  referencia a objeto, o no.
\item Qué petición de dirección podría enviar para la referencia a objeto.
\end{itemize}

Esta información también se obtiene a través de un mensaje de petición
convencional, pero puede que algunos clientes no deseen retransmitir
paquetes grandes cuando el mensaje de respuesta retorna el estado
\id{LOCATION\_FORWARD}.  El cliente puede utilizar el mensaje de
petición de localización para transmitir sus mensajes directamente a
la localización correcta.


El mensaje de petición de localización tiene dos elementos:
\begin{itemize}
\item Una cabecera de mensaje \ac{GIOP}.
\item Una cabecera de petición de localización.
\end{itemize}

\subsubsection{Cabecera de petición de localización}

Está definida como sigue:

\begin{listing}[
  float,
  language=IDL]
module GIOP {
  struct LocateRequestHeader_1_0 {
    unsigned long       request_id;
    sequence<octet>     object_key;
  };
};
\end{listing}

Los miembros tienen el significado habitual.

\subsection{Mensaje de respuesta de localización (\emph{LocateReply})}
Los mensajes de respuesta de localización los envían los servidores a
los clientes como consecuencia de la recepción de un mensaje de
petición de localización.

Un mensaje de respuesta de localización tiene tres elementos,
codificados en el siguiente orden:
\begin{itemize}
\item Una cabecera de mensaje \ac{GIOP}.
\item Una cabecera de respuesta de localización.
\item El cuerpo de la respuesta de localización.
\end{itemize}

\subsubsection{Cabecera del mensaje de respuesta de localización}
Está definido tal como se indica a continuación:

\begin{listing}[language=IDL]
module GIOP {
  enum LocateStatusType_1_0 {
    UNKNOWN_OBJECT,
    OBJECT_HERE,
    OBJECT_FORWARD
  };

  struct LocateReplyHeader_1_0 {
    unsigned long          request_id;
    LocateStatusType_1_0   locate_status;
  };
};
\end{listing}


Los miembros tienen la siguiente definición:
\begin{description}
\item[\id{request\_id}]  Se usa para asociar la respuesta con la petición
  correspondiente.
\item[\id{locate\_status}]  Su valor determina el contenido del cuerpo del
  mensaje.  Los valores que puede tomar son:
  \begin{description}
  \item[\id{UNKNOWN\_OBJECT}]  El objeto especificado en el mensaje de
    petición de localización es desconocido para el servidor.  No se
    incluye cuerpo en el mensaje.
  \item[\id{OBJECT\_HERE}]  El servidor que emite el mensaje de respuesta
    de localización puede recibir directamente peticiones para el objeto
    especificado.  No existe cuerpo en el mensaje.
  \item[\id{OBJECT\_FORWARD}]  El cuerpo del mensaje contiene información.
  \end{description}
\end{description}

\subsubsection{Cuerpo del mensaje de respuesta de localización}
El cuerpo del mensaje está vacío, excepto si el valor del campo
\id{locate\_status} es \id{OBJECT\_FORWARD}.  En ese caso, el cuerpo
contiene una IOR que debe usarse para acceder al objeto especificado
en el mensaje de petición de localización.

El cuerpo del mensaje está serializado siguiendo inmediatamente a la
cabecera.

\subsection{Mensaje de finalización de conexión (\emph{CloseConnection})}
Son mensajes procedentes de los servidores en \ac{GIOP}~1.0 y 1.1.  Este
mensaje informa al cliente de que el servidor pretende cerrar la
conexión y no debería esperar respuestas para las peticiones
futuras.  Además, el cliente no recibirá respuestas para las peticiones
pendientes que ni siquiera serán procesadas.

El mensaje de finalización de conexión está formado únicamente por una
cabecera de mensaje \ac{GIOP} que identifica el tipo de mensaje.

\subsection{Mensaje de error (\emph{MessageError})}
\label{sec:messageError}
El mensaje de error se envía en respuesta a cualquier mensaje \ac{GIOP}
cuyo número de versión o tipo de mensaje sea desconocido para el
receptor del mensaje.  También se puede utilizar como respuesta al
recibir un mensaje con una cabecera mal formada.

El mensaje de error está compuesto únicamente por una cabecera \ac{GIOP}
que identifica el tipo de mensaje.


\section{Transporte de mensajes \acs{GIOP}}
\label{sec:transporteGIOP}
\ac{GIOP} está diseñado para ser implementable sobre una gran variedad de
protocolos.  La definición de \ac{GIOP} hace las siguientes suposiciones
sobre el comportamiento del transporte:

\begin{itemize}
\item El transporte está orientado a conexión.
\item El transporte es fiable.  Específicamente, el transporte
  garantiza que los bytes se entregan en el mismo orden que son
  enviados, al menos una vez, y que existe algún mecanismo que
  permite conocer la entrega exitosa.
\item El transporte puede verse como un flujo de bytes.  No se fuerzan
  limitaciones arbitrarias del tamaño de los mensajes, fragmentación o
  alineamiento.
\item El transporte ofrece notificación de conexiones perdidas.
\item El modelo de transporte para crear conexiones se puede mapear en
  el modelo general de \ac{TCP/IP}.  Un agente publica una dirección de red
  conocida en una \ac{IOR} que el cliente usa cuando inicia la conexión.
\end{itemize}

El servidor no inicia conexiones, pero está preparado para aceptar
peticiones de conexión (\function{accept} en términos de \ac{TCP/IP}).  Otro
agente que conoce la dirección (llamado \emph{cliente}) puede intentar
iniciar conexiones enviando peticiones de conexión a la dirección.  El
servidor puede aceptar la petición, creando una nueva y única conexión
con el cliente, o puede rechazar la petición.  Una vez abierta la
conexión, cualquiera de los dos puede cerrar la conexión.


% Local variables:
%   ispell-local-dictionary: "castellano8"
%   TeX-master: "main.tex"
% End:
