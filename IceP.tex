% -*- coding: utf-8 -*-

\chapter{El protocolo Ice}
\hyperlabel{chap:IceP}
\minitocbox


Este anexo es un resumen del capítulo
\href{http://www.zeroc.com/doc/Ice-3.3.0-IceTouch/manual/Protocol.html}{\fg{The
    Ice Protocol}} de la referencia oficial de ZeroC \ac{Ice} titulada
\href{http://www.zeroc.com/doc/Ice-3.3.0-IceTouch/manual/}{\fg{«Distributed
    Programming with Ice»}}~\cite{Ice_manual}. Se trata de una
traducción parcial del citado capítulo, incluyendo las aclaraciones e
incisos que se han considerado oportunos, pero excluyendo aquellas
partes que quedan fuera de los objetivos del presente trabajo. Con
ello se pretende facilitar al lector una visión detallada de los
aspectos más relevantes del protocolo.

La especificación del protocolo \ac{Ice} consta de tres partes principales:
\begin{itemize}
\item Un conjunto de reglas de codificación para varios tipos de datos.
\item El formato y función de los mensajes que intercambian cliente y
  servidor.
\item Un conjunto de reglas que determinan cómo cliente y servidor
  negocian una versión del protocolo y de la codificación.
\end{itemize}


\section{Codificación de datos}
Los objetivos principales de la codificación de datos de \ac{Ice} son la
simplicidad y la eficiencia. Por ello, se tomaron las siguientes
decisiones de diseño.

\begin{itemize}
\item No se fuerza el alineamiento de tipos básicos con los bordes de
  palabra. Eso simplifica la serialización, evita pérdida de datos y
  cantidad de tráfico en la red.
\item Siempre se utiliza ordenamiento \fg{little~endian} para los
  datos numéricos. Con ello se mejora notablemente la eficiencia de
  servicios de distribución de datos que requieren varios saltos. En
  el caso en el que servidor o cliente se ejecuten en una máquina
  \fg{big~endian} se requiere la transformación, pero ello no supone
  un gran coste dentro del proceso de deserialización y en la
  actualidad la mayoría de las arquitecturas de cómputo son
  \fg{little~endian}.
\end{itemize}


\subsection{Tamaños}

Muchos de los tipos de datos y algunas partes del propio protocolo
tienen un tamaño asociado. Los tamaños se codifican con la siguiente
regla:

\begin{enumerate}
\item Si el número de elementos es menor que 255, la cifra se codifica
  en un solo byte.
\item En otro caso, la cifra se codifica con un byte con valor 255,
  seguido de un entero indicando la cifra propiamente dicha.
\end{enumerate}

Con ello se consigue que para secuencias cortas se ahorren 3 bytes por
cada tamaño, mientras que para secuencias largas el byte adicional
puede ser irrelevante.


\subsection{Encapsulaciones}

Una encapsulación se utiliza para almacenar un tipo de dato de tamaño
variable, pudiendo ser ese tipo desconocido para elementos intermedios
sin que eso impida que pueda seguir su camino hasta el receptor. Las
encapsulaciones se codifican del siguiente modo:

\begin{listing}[
  language = Slice,
  caption  = {Formato de codificación de una \emph{encapsulación} Slice}]
struct Encapsulation {
  int size;
  byte major;
  byte minor;
  // [... size - 6 bytes ...]
};
\end{listing}

El campo \id{size} indica el tamaño de la encapsulación completa
(en bytes). Los campos \id{major} y \id{minor} especifican la
versión de la codificación utilizada para los datos. Al número de
versión le sigue el tamaño de los datos menos los 6 bytes que ocupa la
cabecera de la encapsulación, dado que dicho tamaño ocupa 4 bytes.

La encapsulación es un bloque de datos autónomo que puede ser
reenviado sin necesidad de conocer el formato de los datos que
contiene. Permite anidamiento y es posible enviar encapsulaciones
vacías que tendrán, por tanto, un tamaño de 6 bytes.


\subsection{Slices}
\label{sec:IceP:sizes}

Una \fg{slice} (\emph{rebanada}) es un bloque de bytes precedido por
un tamaño codificado como un entero de 4 bytes. Por lo que un
\fg{slice} vacío ocupa 4 bytes. Tanto las excepciones como las clases
están formadas por \fg{slices}. El objetivo es que al enviar una clase
o excepción con determinada jerarquía de herencia, el receptor pueda
pasar por alto las clases especializadas que no conoce y acceder
 a los datos de los tipos que sí conoce.


\subsection{Tipos básicos}

Los tipos básicos se codifican según se muestra en el
\tablename~\ref{tab:SLICE-basic-types}. Los tipos en punto flotante
usan formatos estándar de \ac{IEEE}\cite{IEEE-float}. Todos los tipos
usan ordenamiento \fg{little~endian}.

\begin{table}[htbp]
  \centering
  \input{tables/SLICE_basic_types.tex}
  \caption{Tamaño de los tipos básicos de Slice}
  \label{tab:SLICE-basic-types}
\end{table}

\subsection{Cadenas}

Las cadenas de texto se codifican con un tamaño
\seesec{IceP:sizes} y, a continuación, el contenido de la cadena
codificado en formato \acs{UTF}~-8~\cite{UTF-8}. Las cadenas no
contienen un valor nulo al final. Una cadena vacía es un tamaño con
valor cero.

\subsection{Tipos agregados}

\subsubsection{Secuencias}

Las secuencias se codifican como un tamaño \seesec{IceP:sizes} que
indica la cantidad de elementos en la secuencia seguido por los
elementos codificados conforme a su tipo.

\subsubsection{Diccionarios}

Los diccionarios se codifican con un tamaño \seesec{IceP:sizes}
que indica la cantidad de pares clave~-valor seguido por dichos
pares. Cada par se codifica como una estructura con dos miembros:
clave y valor, en ese orden.

\subsubsection{Enumeraciones}

La codificación de los enumerados depende de la cantidad de valores
declarados. Hasta 127 valores se codifican como un byte. Más de 127 y
hasta 32767 se codifican como un \id{short}. Si son más, se
codifican como \id{int}. El valor de cada enumerado es el ordinal
que le corresponda comenzando en cero.

\subsubsection{Estructuras}
Los miembros de una estructura se codifican en el mismo orden en el
que aparecen en su declaración y siguiendo las reglas definidas para
sus respectivos tipos.


\subsection{Identidades}
\label{sec:IceP:identity}

Todo objeto \ac{Ice} tiene una identidad de objeto definida del siguiente
modo:

\begin{listing}[
  language = Slice,
  caption =  {Estructura de una \emph{identidad} de objeto \acs{Ice}}]
module Ice {
  struct Identity {
    string name;
    string category;
  };
};
\end{listing}

La identidad consiste en dos cadenas: \id{name} y \id{category}. Para
que dos identidades sean consideradas iguales ambos campos deben
coincidir. La \id{category} normalmente es vacía salvo cuando se
utilizan \fg{Servant locators} (véase \S~28.7 de~\cite{Ice_manual})

\section{Proxies}
\label{sec:IceP:proxy}

El primer campo que se codifica es una identidad (tipo
\iface{Ice::Identity}). El protocolo permite especificar \fg{proxies}
nulos, en cuyo caso se incluye únicamente una identidad con sendas
cadenas vacías para \id{category} y \id{name}
\seesec{IceP:identity}. Si el proxy no es nulo se incluyen un
conjunto de parámetros generales y después pueden aparecer uno o
varios \fg{endpoints}.

La parte común del proxy se codifica como una estructura con los
siguientes campos:

\begin{listing}[
  language = Slice,
  caption =  {Estructura común de un proxy \acs{Ice}}]
struct ProxyData {
  Ice::Identity id;
  Ice::StringSeq facet;
  byte mode;
  bool secure;
};
\end{listing}

El significado de los miembros de esta estructura es el siguiente:
\begin{description}
\item[\id{id}] La identidad de objeto.
\item[\id{facet}] El nombre de la faceta (una secuencia de cero o un
  elemento). La secuencia vacía corresponde con la faceta por defecto.
\item[\id{mode}] El modo del proxy:
  \begin{enumerate}[start=0, itemsep=0pt]
  \item twoway.
  \item oneway.
  \item batch oneway.
  \item datagram.
  \item batch datagram.
  \end{enumerate}
\item[\id{secure}] Es \kw{true} si se requiere un endpoint seguro.
\end{description}

El proxy puede indicar además una lista de endpoints (si es
un proxy directo) o un identificador de adaptador (si es un proxy
indirecto), en este segundo caso se codifica un byte con valor 0 y el
identificador como una cadena.

\subsection{Endpoints}

Los endpoints se codifican inmediatamente después de la
estructura indicada en la sección anterior. Primero, un tamaño indica
el número de endpoints que aparecen después. Cada endpoint se
codifica como un \kw{short} que indica el tipo:

\begin{enumerate}[itemsep=0pt]
\item \ac{TCP}
\item \ac{SSL}
\item \ac{UDP}
\end{enumerate}

y, a continuación, una encapsulación con los parámetros específicos de
cada endpoint.  Esto es así para que receptores que no conocen la
estructura de ciertos tipos de endpoints puedan manejar el proxy
completo a pesar de ello.

\subsubsection{Endpoints \acs{TCP}}
\label{sec:IceP:endpointTCP}

Se codifican por medio de la siguiente estructura:

\begin{listing}[
  language = Slice,
  caption =  {Estructura de un endpoint \acs{TCP}}]
struct TCPEndpointData {
  string host;
  int port;
  int timeout;
  bool compress;
};
\end{listing}

cuyo significado es:
\begin{description}
\item[\id{host}] El nombre del host que aloja el servidor o su dirección IP.
\item[\id{port}] El puerto al que está vinculado el servidor.
\item[\id{timeout}] El timeout especificado para los sockets (en ms).
\item[\id{compress}] Si debe utilizarse compresión.
\end{description}


\subsubsection{Endpoints UDP}

Se codifican según la siguiente estructura:

\begin{listing}[
  language = Slice,
  caption =  {Estructura de un endpoint \acs{UDP}}]
struct UDPEndpointData {
  string host;
  int port;
  byte protocolMajor;
  byte protocolMinor;
  byte encodingMajor;
  byte encodingMinor;
  bool compress;
};
\end{listing}

cuyo significado es:
\begin{description}
\item[\id{host}] El nombre del host que aloja el servidor o su dirección IP.
\item[\id{port}] El puerto al que está vinculado el servidor.
\item[\id{protocolMajor/protocolMinor}] Versión del protocolo
  soportado por el endpoint.
\item[\id{encodingMajor/encodingMinor}] Versión de la codificación
  soportada por el endpoint.
\item[\id{compress}] Si debe utilizarse compresión.
\end{description}


\subsubsection{Endpoints \acs{SSL}}

Se codifican según la siguiente estructura:

\begin{listing}[
  language = Slice,
  caption =  {Estructura de un endpoint \acs{SSL}}]
struct SSLEndpointData {
  string host;
  int port;
  int timeout;
  bool compress;
};
\end{listing}

cuyo significado es idéntico al del endpoint \ac{TCP}
\seesec{IceP:endpointTCP}.

\section{Mensajes del protocolo}

El protocolo \ac{Ice} define 5 tipos de mensajes:

\begin{enumerate}
\item Petición (del cliente al servidor).
\item Petición por lotes (del cliente al servidor).
\item Respuesta (del servidor al cliente).
\end{enumerate}

Y en transportes orientados a conexión:

\begin{enumerate}[resume]
\item Validación de conexión (del servidor al cliente).
\item Cierre de conexión (ambos sentidos).
\end{enumerate}

Los mensajes no requieren ningún tipo de alineamiento y constan de una
cabecera y un cuerpo.

\subsection{Cabecera}

Todos los mensajes del protocolo \ac{Ice} comienzan con una cabecera de 14
bytes según la siguiente estructura:

\begin{listing}[
  language = Slice,
  caption =  {Protocolo \acs{Ice}: Formato de la cabecera}]
struct HeaderData {
  int magic;
  byte protocolMajor;
  byte protocolMinor;
  byte encodingMajor;
  byte encodingMinor;
  byte messageType;
  byte compressionStatus;
  int messageSize;
};
\end{listing}

El significado de cada miembro es el siguiente:


\begin{description}
\item[\id{magic}] Una secuencia de 4 bytes con los códigos
  \ac{ASCII} de los caracteres `I',`c',`e',`P'.
\item[\id{protocolMajor/protocolMinor}] Versión del protocolo.
\item[\id{encodingMajor/encodingMinor}] Versión de la codificación.
\item[\id{messageType}] Tipo de mensaje:
  \begin{enumerate}[start=0, itemsep=0pt]
  \item Petición.
  \item Petición por lotes.
  \item Respuesta.
  \item Validación de conexión.
  \item Cierre de conexión.
  \end{enumerate}
    \item[\id{compressionStatus}] Indica si el mensaje está comprimido.
\item[\id{messageSize}] El tamaño total del mensaje incluyendo la cabecera.
\end{description}

\subsection{Cuerpo del mensaje de petición}
\label{sec:IceP:peticion}

El cuerpo contiene toda la información necesaria para realizar la
invocación a un método de un objeto remoto, lo cual incluye la
identidad del objeto, el nombre de la operación y los parámetros de
entrada. Se codifican conforme a la siguiente estructura:

\begin{listing}[
  language = Slice,
  caption =  {Protocolo \acs{Ice}: Formato del mensaje de petición}]
struct RequestData {
  int requestId;
  Ice::Identity id;
  Ice::StringSeq facet;
  string operation;
  byte mode;
  Ice::Context context;
  Encapsulation params;
};
\end{listing}

siendo:

\begin{description}
\item[\id{requestId}] Identificador de la petición. En las invocaciones
  \fg{oneway} tiene valor 0 e indica que no se debe responder al
  cliente.
\item[\id{id}] Identidad del objeto invocado.
\item[\id{facet}] Nombre de la faceta (cero o un elemento).
\item[\id{operation}] El nombre de la operación invocada.
\item[\id{modo}] Modo de operación: normal (0) o idempotente (2).
\item[\id{context}] Contexto de la invocación.
\item[\id{params}] Parámetros de entrada, en el orden en el que aparecen en
  la declaración de la operación.
\end{description}


\subsection{Cuerpo del mensaje de petición por lotes}

Un mensaje de petición por lotes contiene una o varias invocaciones
\fg{oneway} agrupadas. Se codifica como un entero que indica el
número de invocaciones seguido de los cuerpos de los mensajes de
petición codificados del siguiente modo:

\begin{listing}[
  language = Slice,
  caption =  {Protocolo \acs{Ice}: Formato del mensaje de petición por lotes}]
struct BatchRequestData {
  Ice::Identity id;
  Ice::StringSeq facet;
  string operation;
  byte mode;
  Ice::Context context;
  Encapsulation params;
};
\end{listing}

El significado de los miembros es equivalente al del mensaje de
petición \seesec{IceP:peticion}.


\subsection{Cuerpo del mensaje de respuesta}

El mensaje de respuesta contiene los resultados de una invocación
\fg{twoway} incluyendo el valor de retorno, los parámetros de
salida o el contenido de una posible excepción. Se codifica según la
siguiente estructura.

\begin{listing}[
  language = Slice,
  caption =  {Protocolo \acs{Ice}: Formato del mensaje de respuesta}]
struct ReplyData {
  int requestId;
  byte replyStatus;
  Encapsulation body; // messageSize - 19 bytes
};
\end{listing}

El valor \id{requestId} debe corresponder con el campo del mismo
nombre según se indicó en el mensaje de petición. El campo
\id{replyStatus} indica un tipo de respuesta según la siguiente
lista:

\begin{enumerate}[start=0, itemsep=0pt]
\item Invocación satisfactoria.
\item Excepción de usuario.
\item El objeto no existe.
\item La faceta no existe.
\item La operación no existe.
\item Excepción \ac{Ice} local desconocida.
\item Excepción \ac{Ice} de usuario desconocida.
\item Excepción desconocida.
\end{enumerate}


\subsection{Mensaje de validación de conexión}
\label{sec:Ice:validate}

Cuando un servidor sobre un protocolo orientado a conexión recibe un
intento de conexión siempre responde con un mensaje de
\emph{validación de conexión} sin que el cliente tenga que enviar
nada. El cliente debe esperar este mensaje antes de poder enviar
mensajes de petición. Este mensaje resuelve dos problemas:

\begin{itemize}
\item Informa al cliente de la versión del protocolo y codificación
  soportados por el servidor.
\item Evita que el cliente escriba el mensaje en sus \fg{buffers} locales
  antes de que el servidor haya comprobado que puede atender sus
  peticiones. Eso evita que el servidor acepte nuevas peticiones
  cuando se está apagando.
\end{itemize}


\subsection{Mensaje de cierre de conexión}

Este mensaje lo puede enviar tanto cliente como servidor para indicar
que va a realizar el cierre de la conexión. Solo puede ocurrir cuando
no quedan invocaciones por responder y únicamente cuando se utilizan
transportes twoway.


% Local variables:
%   ispell-local-dictionary: "castellano8"
%   TeX-master: "main.tex"
% End:
