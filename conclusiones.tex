% -*- coding: utf-8 -*-

\chapter{Conclusiones y trabajo futuro}
\hyperlabel{chap:conclusiones}
\minitocbox


\drop{U}{na} vez abordados todos los componentes de la infraestructura
de integración que nos planteábamos como objetivo, llega el momento de
recapitular y analizar las ventajas que se obtienen al fusionar las
soluciones a los problemas parciales que implica el uso de las redes de
sensores bajo nuestro enfoque: el entorno inteligente como un sistema
distribuido heterogéneo.

Este capítulo comienza con la descripción de esa infraestructura,
incluyendo un resumen de los elementos que la forman, y para cada uno
de ellos se destacan las contribuciones concretas que aporta este
trabajo. Por último, se plantean algunas líneas de trabajo que se abren
como consecuencia de dichas aportaciones, y se describen algunas
aplicaciones que pueden ser objeto de investigación en el
futuro.


\section{Infraestructura de integración}

El objetivo era la definición de una infraestructura completa,
flexible y eficaz para resolver la mayoría de los problemas que
aparecen de forma recurrente en la integración de redes \ac{SAN} con
una red troncal y/o entre redes \ac{SAN} de distinta tecnología. Los
principios de diseño para la construcción de esa infraestructura han
sido los siguientes:

\begin{definitionlist}
\item[Integración]
  Adaptar otros \mws, dispositivos o sistemas, incluso cerrados, a
  nuestra infraestructura.

\item[Especialización sin pérdida de generalidad]
  Mediante la definición de interfaces adecuadas, abstrayendo las
  diferencias pero exponiendo la funcionalidad al sistema.

\item[Reutilización]
  Aprovechando cualquier trabajo previo disponible, ya sean
  dispositivos, protocolos, \fg{drivers}, etc.

\item[Interoperabilidad a nivel de protocolo] Consiguiendo de ese modo
  un buen nivel de compatibilidad e interacción transparente entre
  sistemas diferentes.

\item[Nodos autónomos]
  La autonomía de los dispositivos es un aspecto clave para lograr
  tolerancia ante fallos. Propiciar que la inteligencia del sistema esté
  distribuida en lugar de residir en unos pocos componentes.

\item[Orientación a objetos]
  El objeto es la unidad funcional básica para realizar cualquier
  tarea: todo es un objeto. Ese principio permite encapsular la
  funcionalidad, delimitar las responsabilidades sobre datos y
  procesos, y ocultar los detalles específicos.
\end{definitionlist}

La figura~\ref{fig:infraestructura} muestra los elementos que forman
la infraestructura resultante. Los componentes relacionados con esta
tesis (con fondo blanco) y su papel dentro de ella se resumen en el resto de
la sección. Otros componentes de la arquitectura tratan aspectos como
la gestión global de la \ac{QoS} o la composición
automática de servicios, y no han sido abordados. La arquitectura
completa se denomina \acx{DOBS}~\cite{Villanueva09Aframework} y su
objetivo es proporcionar servicios avanzados en entornos inteligentes.

\begin{figure}[h]
\begin{center}
  \includegraphics[width=.8\textwidth]{DOBS-infraestructura.png}
  \caption{Esquema general de la infraestructura de integración de redes
    \acs{SAN}}
  \label{fig:infraestructura}
\end{center}
\end{figure}


\subsection{Middleware orientado a objetos}

No proponemos un nuevo \mw; estamos convencidos que la cantidad y
variedad de \ac{MDOO} de propósito general es suficiente para
encontrar alguno que se adapte razonablemente a la gran mayoría de las
situaciones. Prácticamente todos los \mws{} que se han considerado en
la sección~\ref{sec:prev:mw-para-SAN} proponen planteamientos muy
especializados que implican el diseño de sistemas completamente
nuevos. Sin embargo, en muchos casos llegan a diseños con
abstracciones muy similares a los \ac{MDOO} clásicos. Nuestra
estrategia en este aspecto es más parecida a la de
nORB~\cite{Subramonian03thedesign_nORB}.

Si bien es cierto que las redes \ac{SAN} tienen características y
necesidades especiales, creemos que es mucho más conveniente emplear
un \ac{MDOO} convencional y añadir los servicios y componentes
necesarios para cubrir los casos de uso no considerados en éste que
realizar un nuevo diseño desde cero. Ello supone varias ventajas
importantes:

\begin{definitionlist}
\item[Orientación a objetos] Un paradigma de programación ampliamente
  aceptado y que encaja perfectamente en el diseño e implementación de
  sistemas distribuidos dotando de abstracciones muy convenientes que
  permiten reducir el acoplamiento y aumentar la reutilización por
  medio de interfaces bien definidas a nivel del sistema.

\item[Heterogeneidad] Muchos de estos \mws{} proporcionan bibliotecas o
  \fg{bindings} para ser utilizados desde varios lenguajes de
  programación y están diseñados de modo que es relativamente sencillo
  adaptarlos a nuevas plataformas y sistemas operativos.

\item[Independencia del transporte] Aunque no es una característica
  presente en todos los \ac{MDOO}, la posibilidad de abstraer la
  infraestructura de comunicaciones utilizada para el transporte de
  mensajes inter~-\ac{ORB} resulta especialmente útil.

  En cualquier caso, la necesidad de ese protocolo inter~-\ac{ORB} y
  una representación de datos independiente ofrece una interfaz de
  bajo nivel para interaccionar con el \mw, y esa es precisamente la
  característica que hace posible nuestros \picos.

\item[Servicios comunes] Prácticamente todos los \mws{} veteranos
  disponen de un rico conjunto de servicios perfectamente integrados y
  probados. El desarrollo de cualquier aplicación final se ve a menudo
  favorecido por el ahorro de esfuerzo que suponen los mecanismos ya
  previstos por el fabricante del \mw.

\item[Experiencia] Los principios de diseño en los que se basan los
  \ac{MDOO} se vienen utilizando desde hace tiempo para crear grandes
  aplicaciones. Han demostrado ser una buena solución y adaptarse bien
  a los problemas que han ido apareciendo a lo largo de los años.

  Consideramos de vital importancia aprovechar los diseños,
  desarrollos y experiencia en torno a los sistemas previos. La
  inmensa cantidad de aplicaciones exitosas que se han venido
  desarrollando durante más de una década avalan los \mws{} de
  propósito general, especialmente los \ac{MDOO}.

  Por ejemplo, \ac{CORBA} cuenta con el consenso de cientos de las más
  importantes empresas de la industria del software de todo el mundo.
\end{definitionlist}

En nuestra propuesta, el \mw{} es la piedra angular de la
infraestructura, pero ésta es lo suficientemente flexible como para
adaptarse a \mws{} considerablemente distintos siempre que respeten en
mayor o menor medida el concepto de objeto o servicio distribuido y
estén basados en el intercambio de mensajes o invocación de métodos
remotos.

\subsection{\picos}

Se han descrito y analizado las ventajas prácticas y metodológicas que
implica la construcción de objetos distribuidos empotrados con
\picos. Su utilización en las redes \ac{SAN} ofrece la
oportunidad de que cualquier transductor pueda ser integrado en una
aplicación distribuida de forma completamente transparente, ocultando
todos los detalles de fabricación del dispositivo, su funcionamiento
interno o, incluso, paliando sus limitaciones funcionales cuando se
requiera. Cada transductor es visto \emph{desde la red} como un objeto
autónomo al que se le pueden solicitar servicios sin intermediarios.

Se han analizado en detalle las limitaciones y requisitos de
interoperabilidad para dos casos concretos: \ac{CORBA} e \ac{Ice}, y
se incluye una descripción del juego de instrucciones que permite
describir máquinas de estados para reconocimiento y generación de
mensajes conforme a sus respectivos formatos de protocolo.

Se ha diseñado un lenguaje de alto nivel (IcePick) con el que es
posible modelar servicios como interacciones simples, pero no
triviales, entre objetos de un nodo o entre nodos diferentes,
independientemente de si los objetos involucrados están o no
implementados como \picos. La descripción de interacción entre objetos
de diferentes nodos permite, pero no limita, la definición de
comportamiento global similar al que ofrecen los sistemas de
macro-programación.

El modelo de ejecución que se propone está basado en la utilización de
una máquina virtual y la generación de \fg{bytecode} (aunque no se
descarta la posibilidad de generar código nativo). Se ha diseñado e
implementado un compilador capaz de generar bytecode a partir de
una descripción en IcePick. Proporcionamos implementaciones de
referencia de la máquina virtual para un conjunto de plataformas lo
suficientemente rico como para demostrar su viabilidad sobre
prácticamente cualquier dispositivo de cómputo empotrado conectado a
una red de datos. Se proporciona soporte al flujo de desarrollo
completo de aplicaciones distribuidas en las que haya involucrados
transductores implementados como \picos. Este flujo abarca
desde la especificación de las interfaces hasta las fases de prueba e
implantación.

El uso de bytecode hace posible plantear una solución general para
despliegue y actualización de aplicaciones en los \picos{} por medio
de \picoGrid{} \seesec{picoGrid}.


\subsection{Modelo de información}

\ac{DUO} define un conjunto de servicios rico y flexible, orientado a
satisfacer las necesidades funcionales de la mayoría de las
aplicaciones que involucran redes \ac{SAN} en entornos
inteligentes. Es lo suficientemente sencillo como para ser
implementado sin problema en dispositivos con capacidades muy
reducidas, a la vez que es suficientemente potente como para construir
servicios complejos como es el caso de cámaras de vídeo motorizadas.

Proporciona utilidades para la creación y gestión dinámica de
colecciones de objetos, mecanismos de notificación asíncrona mediante
\emph{objetos activos}, agregación transparente, activación
condicional, etc.

Obviamente, su diseño está muy influenciado por las restricciones para
las que están diseñados los \picos, pero sin que eso suponga
sacrificar sus posibilidades en sistemas o aplicaciones que no los
requieren. Resulta muy útil para disponer de colecciones de objetos
con interfaces muy ortogonales y genéricas, que dan lugar a
características tan interesantes como el \emph{cableado virtual}, que
no serían posibles con interfaces especializadas. Hace posible la
instanciación de servicios reactivos sin más infraestructura que los
propios nodos y la necesaria red de comunicaciones.


\subsection{Descubrimiento de servicios}

\ac{ASDF} complementa el modelo de información de \ac{DUO} aunque no
depende de él. Dota al sistema de mecanismos para la búsqueda y
descubrimiento de servicios. De nuevo, aunque se trata de un protocolo
diseñado teniendo en cuenta las restricciones de los \picos, no
supone una desventaja para su adopción en el sistema
completo. \ac{ASDF} no es un \ac{SDP} para la \ac{SAN} sino para toda
la red, para todos los servicios de la aplicación, involucren o no a
redes de sensores.

Su funcionamiento se fundamenta en una taxonomía de servicios y en la
definición de conjuntos de propiedades. La distribución de información
se realiza por medio de los canales de eventos proporcionados por el
\mw, aunque dispone de una gran diversidad de alternativas para
disposiciones distribuidas, centralizadas o mixtas en los mecanismos
de anuncio, búsqueda y almacenamiento de las propiedades. Esto lo dota
de una gran flexibilidad que le permite adaptarse a las necesidades de
casi cualquier sistema.

También dispone de un servicio de metamodelo que permite a las
aplicaciones explorar programáticamente la descripción de los
servicios de forma que, por ejemplo, un cliente puede listar los tipos
de servicio disponibles o las propiedades válidas en cada uno de ellos
~---algo muy útil para la búsquedas~--- sin necesidad de conocimiento
previo del entorno o la aplicación concreta.


\subsection{Redes heterogéneas}

La interconexión de la red \ac{SAN} con la red troncal se realiza
normalmente por medio de \emph{estaciones base} que operan como
pasarelas de servicio. \ac{IDM} propone una solución alternativa para
lograr encaminamiento independiente de la capa de red, su tecnología o
protocolos.

Esto es posible gracias a que los nodos de la red \ac{SAN} (los
\picos) son autónomos y no requieren entidades ajenas que los
representen o que traduzcan su protocolo, como suele ocurrir en la
gran mayoría de las propuestas previas. Simplifica o soluciona muchos de los
problemas que implicaba el uso de pasarelas
\seesec{prev:pasarelas}. Por ejemplo, hace posible la creación de
redes de sensores \emph{multisink}.

\ac{IDM} es un mecanismo universal de encaminamiento de
mensajes. Aunque ha sido diseñado para solventar los problemas de
acceso a las \ac{SAN}, se ha hecho un esfuerzo considerable para que
pueda aplicarse en cualquier circunstancia en la que se desee comunicar
entre sí nodos de redes incompatibles.

Incluye la definición del formato de los mensajes para el reenvío de
invocaciones, la descripción del procedimiento completo para la
entrega al objeto remoto y la devolución de la respuesta al cliente de
forma transparente.


\section{Un \mw{} para \acs{WSN}}

\note{En qué medida nuestro enfoque es una buena solución para
  \acs{WSN}}
%
Veamos a continuación en qué medida resuelve nuestra propuesta los
problemas de los \mws{} para \ac{SAN} identificados en la
bibliografía. \textsc{Romer}~\cite{Romer02middlewarechallenges}
sugiere una serie de requisitos que, en su experiencia,
debería tener un \mw{} en el contexto concreto de las
\ac{WSN}. Consideramos que es una buena referencia para medir algunos
de los logros conseguidos en esta tesis. Estas son las consideraciones
más importantes:

\emph{«El pequeño tamaño de los nodos implica también recursos
  reducidos (capacidad de cómputo, memoria, alcance y ancho de banda,
  etc.)».}
\begin{quote}
  Una implementación completa de un racimo de \picos, incluyendo la
  máquina virtual, ocupa una pequeña parte de la memoria disponible en
  la mayor parte de los dispositivos utilizados en redes \ac{WSN}, al
  menos un orden de magnitud menos que cualquier sistema previo. Al
  ser una máquina dirigida por eventos (\fg{event-driven}), el consumo
  de \acs{CPU} se produce únicamente cuando es necesario. Los mensajes
  utilizados por las interfaces propuestas son pequeños y se soportan
  protocolos asíncronos, y envío \fg{multicast} cuando la tecnología
  de red lo permite (habitual en redes \fg{wireless}).
\end{quote}


\emph{«La red \ac{WSN} puede estar formada por una gran cantidad
  de nodos diferentes, en términos de transductores utilizados,
  potencia de cómputo y memoria».}
\begin{quote}
  En relación a la ejecución de código, el uso de una máquina virtual
  abstrae las peculiaridades de la arquitectura concreta de cada nodo.

  En las comunicaciones, el protocolo de transporte inter~-\ac{ORB}
  homogeneiza la invocación de cualquier servicio en cualquier nodo.
\end{quote}


\emph{«Comunicación centrada en los datos (\fg{data-centric}). Los
  nodos de la red son, por lo general, meros productores o
  consumidores de datos. Eso mejora la robustez porque desacopla el
  dato del sensor que lo produce».}
\begin{quote}
  Las interfaces \ac{DUO} encajan perfectamente en este enfoque, pero
  además permiten ver los transductores como objetos y proporcionan
  mecanismos para crear colecciones, componentes e integración de
  servicios reactivos por medio de \emph{cableado virtual}.

  Las interfaces \ac{DUO} desacoplan el dato del transductor que lo
  produce, pero el consumidor puede obtener la identidad del objeto
  responsable si lo necesita. Eso permite implementar servicios de
  consulta masiva con una eficiencia razonable.
\end{quote}

\emph{«El \mw{} debe dar soporte al desarrollo, mantenimiento,
  despliegue de aplicaciones».}
\begin{quote}
  Se proporciona un entorno de desarrollo completo
  (capítulo~\ref{chap:designflow}) que permite el desarrollo de
  \picos{} de un modo sencillo y que pueden ser probados antes de su
  implantación, separando claramente los roles del personal implicado
  en el desarrollo.

  Servicios proporcionados por \ac{Ice}, como IceGrid o IcePatch2
  pueden resultar muy útiles para tareas de mantenimiento y despliegue
  de aplicaciones. El despliegue de aplicaciones en la \ac{SAN} se
  aborda con \picoGrid, con un modelo de ejecución que encaja
  perfectamente con el funcionamiento de los \picos.
\end{quote}

\emph{«debe proporcionar [\...] mecanismos para especificar
  tareas de detección complejas, coordinación de sensores para dividir
  la tarea y distribuirla a los nodos sensores individuales, fusión y
  mezcla de los datos a partir de las lecturas individuales para
  conseguir datos de más alto nivel\...».}
\begin{quote}
  El lenguaje IcePick proporciona mecanismos básicos para especificar
  relaciones no triviales entre objetos del mismo o distintos nodos, de
  forma transparente.

  Por medio de canales de eventos y utilizando los servicios que
  proporciona \ac{ASDF} es sencillo construir aplicaciones complejas:
  consultas selectivas, búsquedas por servicio, gestión de propiedades,
  etc. Con todo ello se pueden obtener datos de más alto nivel
  calculados a partir de las lecturas y teniendo en cuenta el contexto
  en que se realiza.

  Estos datos derivados se pueden ofrecer de nuevo a la red por medio
  de sensores virtuales~\cite{Romer02infrastructurefor}. Por ejemplo,
  un objeto \emph{compuesto} \seesec{DUO:compuestos} puede ofrecer la
  temperatura media de una habitación utilizando la misma interfaz que
  los sensores reales que hay en la sala.
\end{quote}

\emph{«El ámbito del \mw{} de la \ac{WSN} no está restringido
  solo a la red, también debe cubrir dispositivos y redes conectados a
  la \ac{WSN}. El \mw{} para la red de sensores debería proporcionar
  una visión holística de la \ac{WSN} y las redes tradicionales, lo
  que supone un desafío para el diseño arquitectural y la
  implementación».}
\begin{quote}
  Como se utiliza un \mw{} de propósito general, para la aplicación no
  hay ninguna diferencia en el acceso a un objeto dentro de una \ac{WSN}
  o de cualquier otro dispositivo en cualquier red accesible.

  Con \ac{IDM} se intenta llevar esta transparencia un paso más
  allá, de modo que se puedan encaminar eventos y peticiones a través de
  cualquier red independientemente de su protocolo o tecnología de
  red. Bajo este enfoque, todos los elementos son objetos distribuidos:
  nodos, transductores, encaminadores, consumidores, etc.

  Aunque ciertamente es un desafío, ya está parcialmente resuelto por
  los \mws{} de comunicaciones para sistemas distribuidos
  heterogéneos. La necesidad aquí es adaptarlos a las características de
  las redes \ac{SAN}, pero no es necesario plantear un diseño
  arquitectural o la implementación de \mws{} completamente nuevos.

  Éste es un aspecto muy importante que plantean también otros
  autores. En~\cite{Verbaeten07asurvey, Horre07OnTheIntegration} se
  clasifican los \mws{} dependiendo si se aplican a la \ac{SAN}, la
  pasarela o la red troncal (\fg{backend}) concluyendo que lo ideal
  sería un \mw{} para todo (\emph{end-to-end}). Ese es precisamente
  nuestro planteamiento inicial y creemos que se ha logrado en buena
  medida.
\end{quote}


\section{Resumen de contribuciones}

Según se desprende de las secciones anteriores esta tesis aporta las
siguientes contribuciones específicas:
\begin{itemize}
\item Una infraestructura de integración que resuelve los problemas de
  las redes \ac{SAN} en un contexto heterogéneo de dispositivos, redes
  y servicios.
\item Un \mw{} construido sobre cualquier MDOO de propósito
  general que aprovecha las ventajas de las soluciones estándar para
  computación distribuida heterogénea.
\item Un conjunto de métodos, lenguajes, y herramientas de soporte
  para el desarrollo de objetos distribuidos diminutos, como mínimo un
  orden de magnitud más pequeños que las aproximaciones anteriores.
\item Un modelo de información rico y flexible orientado al diseño de
  aplicaciones que involucran redes \ac{SAN}.  El modelo soporta una
  amplia variedad de modos de interacción, jerarquía, agregación,
  activación condicional e introspección avanzada.
\item Un mecanismo de anunciamiento y descubrimiento de servicios que
  permite integrar muy fácilmente redes heterogéneas e incluso islas
  gestionadas con \ac{SDP} incompatibles entre sí.  El mecanismo se
  sustenta en una taxonomía que define el conjunto de propiedades y un
  metamodelo que proporciona mecanismos de introspección.
\item Un mecanismo de encaminamiento heterogéneo de mensajes, que
  resuelve problemas de acceso a las \ac{SAN}, pero que también puede
  ser usado para proporcionar calidad de servicio, incrementar la
  tolerancia a fallos, proporcionar transparencia de migración, etc.
\end{itemize}

Todas estas contribuciones responden al diseño de una plataforma
unificada para la construcción de aplicaciones, servicios e
instalaciones que involucren a redes \ac{SAN}, incluso las que
integran dispositivos de muy bajo coste.  De esta forma se proporciona
una solución a la mayoría de los problemas identificados con
anterioridad.

\section{Publicaciones}
\label{sec:publicaciones}

Derivados de los trabajos de esta tesis se han publicado algunos trabajos
relevantes; bien como consecuencia directa de las aportaciones,
bien como trabajo conjunto con otros miembros del grupo \ac{ARCO}:

En~\cite{Villa04PicoCORBA} se plantea la posibilidad de generar
autómatas reconocedores para mensajes \ac{GIOP} y de ese modo
conseguir interoperabilidad básica con clientes \ac{CORBA}. Se
analiza en detalle cómo salvar los problemas de interoperabilidad con
el formato de los mensajes.

En~\cite{Villa05Spreading} se generaliza el planteamiento para dar
cabida a cualquier protocolo inter-\ac{ORB} binario, dando detalles
específicos tanto de \ac{CORBA} como \ac{Ice}. Por último,
en~\cite{Villa06Embedding} se aportan prototipos funcionales de
picoCORBA para \ac{PC} (en C y Java), \ac{TINI} y PIC12C509 (asm) y se
comparan con prototipos de otros \mws{} empotrados.
En~\cite{Villanueva07Lightweight} se describen las ventajas que el
flujo de diseño con \picos{} aporta al desarrollo de aplicaciones
ubicuas y sus posibilidades de implementación mediante lógica
reconfigurable. En~\cite{Moya09Embeddingstandard} planteamos una
solución completa para el uso de \ac{MDOO} en redes de sensores
incluyendo la posibilidad de implementar objetos distribuidos tanto en
microcontroladores de gama baja como en dispositivos \ac{FPGA}.

También es destacable la solicitud de patente titulada «Método y
dispositivo para interconexión de equipos heterogéneos con recursos
limitados por medio de middlewares orientados a objeto»
(Ref:200803715) que en estos momentos se encuentra pendiente de
aprobación.

En~\cite{Villanueva05aQoSframework, Villanueva06ContextAware} se
aborda el problema de la gestión de calidad de servicio en redes
móviles ad hoc (\acs{MANET}) en el contexto de los entornos
inteligentes. En~\cite{Villanueva06Multimedia} se tratan los problemas
de transmisión de flujos multimedia en este tipo de redes.

El protocolo \ac{ASDF} y sus características como \ac{SDP} adecuado
para redes de sensores se discuten en \cite{Villa07Minimalist,
  Villa08ASDF}. Se analizan los casos de uso soportados y los
diferentes escenarios posibles que cubren la funcionalidad típica de
los \acsp{SDP} más utilizados.

\ac{IDM} y su aplicación para obtener acceso transparente a las redes
de sensores se aborda en~\cite{Villa08IDM} explicando los problemas de
interoperabilidad y robustez que implican las pasarelas de servicios y
cómo este enfoque ayuda a evitarlas. En~\cite{Urzaiz09Anovel} se
aplican estos principios a la comunicación con redes de vehículos
submarinos.

Los métodos y herramientas desarrollados para la generación de
\picos{} tienen también aplicación a middlewares no orientados a
objeto, como es el caso de los Web Services.  En
~\cite{Villa09WebServices} se explora esta posibilidad de forma
análoga a como se describe en el anexo~\ref{chap:ews}.  Se incide
en los procesos necesarios para dotar de autonomía a los nodos de una
red de sensores que implementan por sí mismos todo lo necesario para
proporcionar un servicio extremo a extremo.


\section{Nuevas líneas de trabajo}

El trabajo realizado proporciona nuevos enfoques para la resolución de
problemas, o bien para mejorar o simplificar otros ya
resueltos. Algunos de ellos se describen a continuación.


\subsection{Encaminamiento ad hoc con \acs{IDM}}
\label{sec:idm-en-san}

Si bien \ac{IDM} se ha concebido como una solución de interconexión de
redes heterogéneas, se puede utilizar para conseguir encaminamiento en
una red ad hoc (muy habitual en redes \ac{SAN}). En este caso, cada
nodo de la red debe incluir un encaminador \ac{IDM} y un servicio que
proporcione descubrimiento de vecinos mediante \ac{ALP}. Este
planteamiento tiene varias ventajas:

\begin{itemize}
\item El encaminamiento de invocaciones dentro y fuera de la red ad
  hoc es transparente. Para los objetos del nodo no hay diferencia
  entre invocar un objeto en otro nodo de la red ad hoc, la red
  troncal o una tercera red. Lo mismo puede aplicarse a un cliente en la red
  troncal.

\item Es relativamente sencillo plantear soluciones de encaminamiento
  para redes híbridas. En estos momentos ya estamos trabajando en un
  escenario en el que una red de \acp{PDA} que forman una red ad hoc
  se comunica con redes de sensores y con una red troncal estática en
  la que puede haber computadores portátiles. Las invocaciones desde
  cualquier nodo, sea computador, \ac{PDA} o nodo de la \ac{SAN}
  pueden llegar a cualquier otro dispositivo encaminándose a través de
  cualquiera de las tres redes de forma transparente, gracias a
  \ac{IDM}. El protocolo de encaminamiento (basado en \ac{AODV}) se
  implementa sobre \ac{IDM}.

\item Es posible acceder fácil y remotamente a la información de
  encaminamiento dado que los encaminadores \ac{IDM} son objetos
  distribuidos. No se necesitan mecanismos adicionales, el mismo \mw{}
  utilizado para acceder a los servicios se emplea para
  transportar los mensajes del protocolo de encaminamiento y acceder a
  los propios encaminadores.

\item Los encaminadores \ac{IDM} se pueden implementar como \picos. En
  ese caso, las capacidades de los nodos deberían ser mayores que las
  consideradas para servir transductores. El encaminador puede
  requerir una cantidad relativamente importante de memoria \ac{RAM}
  (en relación a los nodos sensores) para el mantenimiento de la tabla
  de rutas y el estado del algoritmo de encaminamiento. Los 128 bytes
  de \ac{RAM} que eran suficientes para un \pico{} resultan
  insuficientes en este caso. Incluso requiriendo una plataforma de 16
  bits, el tamaño de estos encaminadores podría ser ostensiblemente
  menor que el de otras propuestas actuales y con la gran ventaja de permitir
  encaminamiento transparente fuera de la red.
\end{itemize}


\subsection{Plataforma de gestión para redes heterogéneas}
\label{sec:plat-de-gestion}

Siguiendo nuestro planteamiento habitual de que \emph{todo es un
  objeto}, proponemos el modelado de cualquier nodo de la red como un
conjunto de objetos. No sólo sus transductores; cada componente
funcional del \fg{host} puede ser representado como un objeto, siguiendo un
modelo de información similar a \ac{DUO}. Por ejemplo, la memoria
disponible, la energía restante o el número de mensajes recibidos
podrían obtenerse remotamente por medio de invocaciones a métodos. No
es una propuesta nueva. El modelo de información de
\acx{TMN}~\cite{union01:_telec_manag_networ} no dista mucho de esta
idea. La ventaja aquí es que nuestra plataforma de gestión podría
estar implementada como \picos{} (cuando las limitaciones lo
requieran), aunque eso no debería impedir su adaptación a cualquier
otro nodo, incluso a \acp{PC}.

Una plataforma de este tipo sería muy útil para analizar y depurar
algoritmos de encaminamiento o protocolos de comunicaciones
experimentales en redes de dispositivos empotrados. Únicamente las
variables a controlar serían realmente instaladas en el dispositivo, y
con la posibilidad de utilizar \picoGrid{} para desplegar versiones
nuevas para cambiar el conjunto de variables accesibles.


\subsection{Plataforma de despliegue heterogéneo}

En redes formadas por muy diversos dispositivos resultaría muy útil un
servicio de despliegue único que permita enviar actualizaciones de
configuración y programas de forma completamente independiente de la
tecnología del nodo y de la red a la que esté conectado. La solución
pasaría por integrar un servicio de despliegue para nodos
convencionales (como IceGrid) junto con \picoGrid, empleando
\ac{IDM} como medio de comunicación para homogeneizar el acceso a
cualquier dispositivo.


\subsection{Modelado semántico de servicios}

Nuestra experiencia en el uso del protocolo de búsqueda de servicios
que proporciona \ac{ASDF} ha puesto en evidencia algunos problemas
principalmente en cuanto a flexibilidad. El más grave es que al
emplear una taxonomía como \emph{diccionario} de servicios,
las posibilidades de incluir nuevos tipos de servicios en un sistema
en operación son muy limitadas. Esto normalmente no es un problema si
todos los servicios están definidos en la fase de diseño, pero sí puede
serlo en otro caso.

Se propone la definición de un modelo semántico, expresado mediante
una ontología y descrito mediante un lenguaje formal, que permite a
servicios y dispositivos definir nuevas instancias en contextos
específicos. De este modo, cuando un cliente necesita un servicio
envía una consulta que especifica un conjunto de enunciados que se
deben cumplir, por ejemplo «\code{captar imagen}» y se puede acompañar
de propiedades como la ubicación o la disponibilidad. El diccionario
de verbos (\emph{captar}) y objetos (\emph{imagen}) puede estar
disponible a través de un servicio específico. Las descripciones de
los servicios y el vocabulario pueden cambiar incluso en un sistema en
funcionamiento.

Realmente no implica grandes cambios respecto a las propuestas
realizadas en esta tesis. Seguimos teniendo propiedades tal como se
han implementado en \ac{ASDF}, un servicio de metamodelo para servir
los vocabularios y las relaciones (\ac{MIS}) y una descripción del
servicio alojada en cada actor. Solo el método
\function{ASD::Search::lookup()} necesita ser modificado para incluir
una descripción de servicio más compleja, en lugar de simplemente una
interfaz y un conjunto de propiedades. El resto del protocolo
\ac{ASDF} funcionaría tal como se ha explicado, pero la flexibilidad,
riqueza y precisión de las búsquedas aumentaría sensiblemente.


% Local variables:
%   ispell-local-dictionary: "castellano8"
%   TeX-master: "main.tex"
% End:
